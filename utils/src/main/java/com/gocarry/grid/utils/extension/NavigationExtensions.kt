package com.gocarry.grid.utils.extension

import android.app.TaskStackBuilder
import android.content.Intent
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity

fun AppCompatActivity.navigateUp() {
    val upIntent = NavUtils.getParentActivityIntent(this)
    if (NavUtils.shouldUpRecreateTask(this, upIntent!!) || isTaskRoot) {
        TaskStackBuilder.create(this).addNextIntentWithParentStack(upIntent).startActivities()
    } else {
        upIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        NavUtils.navigateUpTo(this, upIntent)
    }
}