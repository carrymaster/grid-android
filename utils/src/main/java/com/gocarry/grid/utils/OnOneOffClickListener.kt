package com.gocarry.grid.utils

import android.os.Handler
import android.os.SystemClock
import android.view.View

abstract class OnOneOffClickListener : View.OnClickListener {

    private var mLastClickTime: Long = 0

    abstract fun onSingleClick(v: View)

    override fun onClick(v: View) {
        val currentClickTime = SystemClock.uptimeMillis()
        val elapsedTime = currentClickTime - mLastClickTime

        mLastClickTime = currentClickTime

        if (elapsedTime <= MIN_CLICK_INTERVAL)
            return
        if (!isViewClicked) {
            isViewClicked = true
            startTimer()
        } else {
            return
        }
        onSingleClick(v)
    }

    private fun startTimer() {
        val handler = Handler()
        handler.postDelayed(Runnable { isViewClicked = false }, 600)
    }

    companion object {
        private const val MIN_CLICK_INTERVAL: Long = 600
        var isViewClicked = false
    }

}