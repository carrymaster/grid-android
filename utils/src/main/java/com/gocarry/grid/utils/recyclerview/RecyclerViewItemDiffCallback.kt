package com.gocarry.grid.utils.recyclerview

import android.support.v7.util.DiffUtil

class RecyclerViewItemDiffCallback(private val oldItems: List<RecyclerViewItem>,
                                   private val newItems: List<RecyclerViewItem>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldItems[oldItemPosition].areItemTheSame(newItems[newItemPosition])

    override fun getOldListSize() = oldItems.size

    override fun getNewListSize() = newItems.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition].areContentTheSameOf(newItems[newItemPosition])
    }

}