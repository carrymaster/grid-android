package com.gocarry.grid.utils

import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KClass

@Suppress("UNCHECKED_CAST")
object InstanceCache {
    private val instances = ConcurrentHashMap<KClass<*>, Any>()

    fun <T> instance(clazz: KClass<*>, createInstanceLambda: () -> T): T {
        return instances.getOrPut(clazz) { createInstanceLambda() } as T
    }

    fun <T> get(clazz: KClass<*>): T {
        return instances[clazz] as T
    }

    fun flush() {
        instances.clear()
    }

}