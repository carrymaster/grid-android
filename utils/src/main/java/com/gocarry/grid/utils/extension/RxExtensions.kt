package com.gocarry.grid.utils.extension

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

//fun <T> Single<T>.logOnError(tag: String): Single<T> =
//        doOnError { ExceptionLogger.log(tag, it) }
//
//fun <T> Maybe<T>.logOnError(tag: String): Maybe<T> =
//        doOnError { ExceptionLogger.log(tag, it) }
//
//fun Completable.logOnError(tag: String): Completable =
//        doOnError { ExceptionLogger.log(tag, it) }
//
//fun <T> Observable<T>.logOnError(tag: String): Observable<T> =
//        doOnError { ExceptionLogger.log(tag, it) }


fun Completable.onDefaultSchedulers(): Completable =
        observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread())

fun <T> Single<T>.onDefaultSchedulers(): Single<T> =
        observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread())

fun <T> Maybe<T>.onDefaultSchedulers(): Maybe<T> =
        observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread())

fun <T> Observable<T>.onDefaultSchedulers(): Observable<T> =
        observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.newThread())