package com.gocarry.grid.utils.recyclerview

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

interface RecyclerViewFactory {
    fun getViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder
}
