package com.gocarry.grid.utils.recyclerview

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

class RecyclerViewAdapter(private val factory: RecyclerViewFactory)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: MutableList<RecyclerViewItem> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            factory.getViewHolder(parent, viewType)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        items[position].bind(holder)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun updateWith(newItems: List<RecyclerViewItem>) {
        val result = calculateDiff(newItems)
        items = newItems.toMutableList()
        result.dispatchUpdatesTo(this)
    }

    private fun calculateDiff(newItems: List<RecyclerViewItem>) =
            DiffUtil.calculateDiff(RecyclerViewItemDiffCallback(items, newItems))

    override fun getItemViewType(position: Int): Int {
        return items.getOrNull(position)?.itemViewType ?: -1
    }

    fun isEmpty() = items.isEmpty()

}

