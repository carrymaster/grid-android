package com.gocarry.grid.utils.recyclerview

import android.support.v7.widget.RecyclerView

interface RecyclerViewItem {
    val itemViewType: Int
    fun bind(viewHolder: RecyclerView.ViewHolder)
    fun areItemTheSame(otherItem: RecyclerViewItem) = false
    fun areContentTheSameOf(otherItem: RecyclerViewItem) = false
}
