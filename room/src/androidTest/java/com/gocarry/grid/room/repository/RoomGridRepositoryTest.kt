package com.gocarry.grid.room.repository

import com.gocarry.grid.core.domain.Grid
import com.gocarry.grid.room.RoomTest
import com.gocarry.grid.test.core.domain.factory.GridFactory
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test

class RoomGridRepositoryTest : RoomTest() {

    private val grid = GridFactory.create()
    private val anotherGrid = GridFactory.create(id = "anotherGrid")
    private val userIdSet get() = grid.balanceSet.first().userIds

    private val roomGridRepository by lazy { RoomGridRepository(database) }
    private lateinit var putTestObserver: TestObserver<Void>
    private lateinit var removeTestObserver: TestObserver<Void>
    private lateinit var findAllTestObserver: TestObserver<List<Grid>>

    @Test
    fun findAllGrids() {
        givenTwoGrids()

        whenFindAll()

        thenTwoGridsAreReturned()
    }

    @Test
    fun findAllGridsWhenAnyExist() {
        whenFindAll()

        thenAnEmptyListIsReturned()
    }

    @Test
    fun addingNewGrid() {
        whenPutGrid()
        thenGridIsIsAdded()
    }

    @Test
    fun removeAGrid() {
        givenAGrid()

        whenDeleteIt()

        thenGridIsDeleted()
    }

    private fun givenTwoGrids() {
        roomGridRepository.put(grid).blockingAwait()
        roomGridRepository.put(anotherGrid).blockingAwait()
    }

    private fun givenAGrid() {
        roomGridRepository.put(grid).blockingAwait()
    }

    private fun whenDeleteIt() {
        removeTestObserver = roomGridRepository.remove(grid.id).test().await()
    }

    private fun whenFindAll() {
        findAllTestObserver = roomGridRepository.findAll().test().await()
    }

    private fun whenPutGrid() {
        putTestObserver = roomGridRepository.put(grid).test()
    }

    private fun thenGridIsIsAdded() {
        val grid = roomGridRepository.find(grid.id).blockingGet()
        putTestObserver.assertComplete()
        then(grid.id).isEqualTo(this.grid.id)
        then(grid.description).isEqualTo(this.grid.description)
        then(grid.userCount).isEqualTo(this.grid.userCount)
        then(grid.findBalance(userIdSet)).isNotNull
        then(grid.findBalance(userIdSet)!!.amount).isEqualTo(this.grid.findBalance(userIdSet)!!.amount)
        then(grid.findBalance(userIdSet)!!.userBalances).isEqualTo(this.grid.findBalance(userIdSet)!!.userBalances)
    }

    private fun thenGridIsDeleted() {
        val gridFound = roomGridRepository.find(grid.id).blockingGet()
        removeTestObserver.assertComplete()
        then(gridFound).isNull()
    }

    private fun thenTwoGridsAreReturned() {
        val gridsFound = findAllTestObserver.values().first()
        findAllTestObserver.assertComplete()
        then(gridsFound.find { it.id == grid.id }).isNotNull
        then(gridsFound.forEach { it.id == anotherGrid.id }).isNotNull
    }

    private fun thenAnEmptyListIsReturned() {
        val gridsFounds = findAllTestObserver.values().first()
        findAllTestObserver.assertComplete()
        then(gridsFounds).isEmpty()
    }
}
