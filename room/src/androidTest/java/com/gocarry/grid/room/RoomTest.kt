package com.gocarry.grid.room

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import com.gocarry.grid.room.database.GridRoomDatabase
import org.junit.After
import org.junit.Rule

abstract class RoomTest {

    @Rule
    @JvmField
    val instantTaskExecuteRule = InstantTaskExecutorRule()

    internal val database: GridRoomDatabase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(), GridRoomDatabase::class.java)
            .allowMainThreadQueries()
            .build()

    @After
    fun tearDown() {
        database.close()
    }

}