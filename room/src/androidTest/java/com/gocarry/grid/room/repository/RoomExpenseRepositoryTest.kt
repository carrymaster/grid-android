package com.gocarry.grid.room.repository

import com.gocarry.grid.room.RoomTest
import com.gocarry.grid.test.core.domain.factory.ExpenseFactory
import com.gocarry.grid.test.core.domain.factory.FactoryDefaultValues.expenseId
import com.gocarry.grid.test.core.domain.factory.GridFactory
import com.gocarry.grid.test.core.domain.factory.PaymentFactory
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test

class RoomExpenseRepositoryTest : RoomTest() {

    private val grid = GridFactory.create()
    private val expense = ExpenseFactory.create(id = expenseId(1))
    private val anotherExpense = ExpenseFactory.create(id = expenseId(2))
    private val updatedPaymentList = expense.payments.map { PaymentFactory.create(it.userId, 5f) }
    private val updatedDescription = "updated description"
    private val updatedExpense = ExpenseFactory.create(id = expenseId(1), paymentList = updatedPaymentList, description = updatedDescription)

    private val gridRepository = RoomGridRepository(database)
    private val expenseRepository = RoomExpenseRepository(database)

    private lateinit var addExpenseTestObserver: TestObserver<Void>
    private lateinit var updateExpenseTestObserver: TestObserver<Void>
    private lateinit var removeExpenseTestObserver: TestObserver<Void>
    private lateinit var removeByGridTestObserver: TestObserver<Void>

    @Test
    fun addAnExpense() {
        givenAGrid()

        whenAddAnExpense()

        thenExpenseIsAdded()
    }

    @Test
    fun updateAnExpense() {
        givenAGrid()
        givenAnExpense()

        whenUpdateAnExpense()

        thenExpenseIsUpdated()
    }

    @Test
    fun removeAnExpense() {
        givenAGrid()
        givenAnExpense()

        whenRemoveExpense()

        thenExpenseIsRemoved()
    }

    @Test
    fun removeAllExpensesInAGrid() {
        givenAGrid()
        givenTwoExpenses()

        whenRemoveAll()

        thenExpensesAreDeleted()
    }

    private fun givenTwoExpenses() {
        expenseRepository.put(expense).blockingAwait()
        expenseRepository.put(anotherExpense).blockingAwait()
    }

    private fun givenAGrid() {
        gridRepository.put(grid).blockingAwait()
    }

    private fun givenAnExpense() {
        expenseRepository.put(expense).blockingAwait()
    }

    private fun whenAddAnExpense() {
        addExpenseTestObserver = expenseRepository.put(expense).test().await()
    }

    private fun whenUpdateAnExpense() {
        updateExpenseTestObserver = expenseRepository.update(updatedExpense).test().await()
    }

    private fun whenRemoveAll() {
        removeByGridTestObserver = expenseRepository.removeByGrid(grid.id).test().await()
    }

    private fun whenRemoveExpense() {
        removeExpenseTestObserver = expenseRepository.remove(expense.id).test().await()
    }

    private fun thenExpenseIsRemoved() {
        val expenseFound = expenseRepository.find(expense.id).blockingGet()
        removeExpenseTestObserver.assertComplete()
        then(expenseFound).isNull()
    }

    private fun thenExpensesAreDeleted() {
        val expenseFound = expenseRepository.find(expense.id).blockingGet()
        val anotherExpenseFound = expenseRepository.find(anotherExpense.id).blockingGet()
        removeByGridTestObserver.assertComplete()
        then(expenseFound).isNull()
        then(anotherExpenseFound).isNull()
    }

    private fun thenExpenseIsAdded() {
        val expenseFound = expenseRepository.find(expense.id).blockingGet()
        addExpenseTestObserver.assertComplete()
        then(expenseFound).isEqualTo(expense)
    }

    private fun thenExpenseIsUpdated() {
        val expenseFound = expenseRepository.find(expense.id).blockingGet()
        updateExpenseTestObserver.assertComplete()
        then(expenseFound).isEqualTo(updatedExpense)
    }
}
