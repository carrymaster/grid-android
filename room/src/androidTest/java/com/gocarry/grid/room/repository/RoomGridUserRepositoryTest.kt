package com.gocarry.grid.room.repository

import com.gocarry.grid.core.domain.GridUser
import com.gocarry.grid.room.RoomTest
import com.gocarry.grid.test.core.domain.factory.FactoryDefaultValues.gridUserId
import com.gocarry.grid.test.core.domain.factory.GridFactory
import com.gocarry.grid.test.core.domain.factory.GridUserFactory
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test

class RoomGridUserRepositoryTest : RoomTest() {

    private val grid = GridFactory.create()
    private val anotherGrid = GridFactory.create(id = "anotherGridId")
    private val gridUser = GridUserFactory.create(id = gridUserId(1))
    private val anotherGridUser = GridUserFactory.create(id = gridUserId(2))
    private val gridUserFromAnotherGrid = GridUserFactory.create(id = gridUserId(3), gridId = anotherGrid.id)
    private val roomGridRepository by lazy { RoomGridRepository(database) }
    private val roomGridUserRepository by lazy { RoomGridUserRepository(database) }

    private lateinit var removeTestObserver: TestObserver<Void>
    private lateinit var findTestObserver: TestObserver<List<GridUser>>

    @Test
    fun findAllByGrid() {
        givenAGrid()
        givenAnotherGrid()
        givenTwoGridUsers()
        givenAGridUserFromAnotherGrid()

        whenFindAllByGridId()

        thenOnlyCorrectGridUsersAreReturned()
    }

    @Test
    fun addAGridUser() {
        givenAGrid()
        whenAddGridUser()
        thenGridUserIsAdded()
    }

    @Test
    fun addTwoGridUsers() {
        givenAGrid()
        whenAddTwoGridUsers()
        thenTwoGridUsersAreAdded()
    }

    @Test
    fun removeGridUserById() {
        givenAGrid()
        givenAGridUser()
        whenDeleteIt()
        thenItAreDeleted()
    }

    @Test
    fun removeGridUsersFromGrid() {
        givenAGrid()
        givenTwoGridUsers()
        whenDeleteThem()
        thenGridUsersAreDeleted()
    }

    private fun givenAnotherGrid() {
        roomGridRepository.put(anotherGrid).blockingAwait()
    }

    private fun givenAGridUser() {
        roomGridUserRepository.put(gridUser).blockingAwait()
    }

    private fun givenAGridUserFromAnotherGrid() {
        roomGridUserRepository.put(gridUserFromAnotherGrid).blockingAwait()
    }

    private fun givenTwoGridUsers() {
        roomGridUserRepository.putAll(listOf(gridUser, anotherGridUser)).blockingAwait()
    }

    private fun givenAGrid() {
        roomGridRepository.put(grid).blockingAwait()
    }

    private fun whenAddTwoGridUsers() {
        roomGridUserRepository.putAll(listOf(gridUser, anotherGridUser)).blockingAwait()
    }

    private fun whenFindAllByGridId() {
        findTestObserver = roomGridUserRepository.findAllByGrid(grid.id).test().await()
    }

    private fun whenDeleteIt() {
        removeTestObserver = roomGridUserRepository.remove(gridUser.id, gridUser.gridId).test().await()
    }

    private fun whenDeleteThem() {
        removeTestObserver = roomGridUserRepository.removeByGrid(grid.id).test().await()
    }

    private fun whenAddGridUser() {
        roomGridUserRepository.put(gridUser).blockingAwait()
    }

    private fun thenTwoGridUsersAreAdded() {
        validateGridUser(gridUser)
        validateGridUser(anotherGridUser)
    }

    private fun thenGridUsersAreDeleted() {
        val gridUserFound = roomGridUserRepository.find(gridUser.id, grid.id).blockingGet()
        val anotherGridUserFound = roomGridUserRepository.find(anotherGridUser.id, grid.id).blockingGet()
        removeTestObserver.assertComplete()
        then(gridUserFound).isNull()
        then(anotherGridUserFound).isNull()
    }

    private fun thenGridUserIsAdded() {
        validateGridUser(gridUser)
    }

    private fun thenItAreDeleted() {
        val gridUserFound = roomGridUserRepository.find(gridUser.id, gridUser.gridId).blockingGet()
        removeTestObserver.assertComplete()
        then(gridUserFound).isNull()
    }

    private fun thenOnlyCorrectGridUsersAreReturned() {
        val gridUsersFound = findTestObserver.values().first()
        findTestObserver.assertComplete()
        then(gridUsersFound).containsExactlyInAnyOrder(gridUser, anotherGridUser)
        then(gridUsersFound).doesNotContain(gridUserFromAnotherGrid)
    }

    private fun validateGridUser(gridUser: GridUser) {
        val gridUserFound = roomGridUserRepository.find(gridUser.id, gridUser.gridId).blockingGet()
        then(gridUserFound).isEqualTo(gridUser)
    }

}
