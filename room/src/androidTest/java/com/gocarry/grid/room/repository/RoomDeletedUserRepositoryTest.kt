package com.gocarry.grid.room.repository

import com.gocarry.grid.room.RoomTest
import com.gocarry.grid.test.core.domain.factory.DeletedUserFactory
import com.gocarry.grid.test.core.domain.factory.FactoryDefaultValues
import com.gocarry.grid.test.core.domain.factory.GridFactory
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test

class RoomDeletedUserRepositoryTest : RoomTest() {

    private val grid = GridFactory.create()
    private val deletedUser = DeletedUserFactory.create()
    private val anotherDeletedUser = DeletedUserFactory.create(id = FactoryDefaultValues.gridUserId(2))

    private val gridRepository = RoomGridRepository(database)
    private val deletedUserRepository = RoomDeletedUserRepository(database)
    private lateinit var addTestObserver: TestObserver<Void>
    private lateinit var removeTestObserver: TestObserver<Void>

    @Test
    fun addDeletedUser() {
        givenAGrid()
        whenAddDeletedUser()
        thenUserIsAdded()
    }

    @Test
    fun addDeletedUserWithWronGrid() {
        whenAddDeletedUser()
        thenAddUserFails()
    }

    @Test
    fun deleteByGridId() {
        givenAGrid()
        givenTwoDeledUsers()
        whenDeleteByGridId()
        thenAllAreDeleted()
    }

    private fun givenTwoDeledUsers() {
        deletedUserRepository.put(deletedUser).blockingAwait()
        deletedUserRepository.put(anotherDeletedUser).blockingAwait()
    }

    private fun givenAGrid() {
        gridRepository.put(grid).blockingAwait()
    }

    private fun whenAddDeletedUser() {
        addTestObserver = deletedUserRepository.put(deletedUser).test().await()
    }

    private fun whenDeleteByGridId() {
        removeTestObserver = deletedUserRepository.removeByGrid(grid.id).test().await()
    }

    private fun thenUserIsAdded() {
        val deletedUserFound = deletedUserRepository.find(deletedUser.id, deletedUser.gridId).blockingGet()
        addTestObserver.assertComplete()
        then(deletedUserFound).isNotNull
        then(deletedUserFound).isEqualTo(deletedUser)
    }

    private fun thenAddUserFails() {
        val deletedUserFound = deletedUserRepository.find(deletedUser.id, deletedUser.gridId).blockingGet()
        addTestObserver.assertNotComplete()
        then(deletedUserFound).isNull()
    }

    private fun thenAllAreDeleted() {
        val deletedUserFound = deletedUserRepository.find(deletedUser.id, deletedUser.gridId).blockingGet()
        val anotherDeletedUserFound = deletedUserRepository.find(anotherDeletedUser.id, anotherDeletedUser.gridId).blockingGet()
        removeTestObserver.assertComplete()
        then(deletedUserFound).isNull()
        then(anotherDeletedUserFound).isNull()
    }
}
