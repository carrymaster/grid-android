package com.gocarry.grid.room.service

import com.gocarry.grid.room.RoomTest
import com.gocarry.grid.room.repository.RoomGridRepository
import com.gocarry.grid.test.core.domain.factory.GridFactory
import io.reactivex.Completable
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test

class RoomTransactionServiceTest : RoomTest() {

    private val gridOne = GridFactory.create(id = "gridIdOne")
    private val gridTwo = GridFactory.create("gridIdTwo")

    private val gridRepository = RoomGridRepository(database)
    private lateinit var operation: () -> Unit

    private val transactionService = RoomTransactionService(database)
    private lateinit var transactionTestObserver: TestObserver<Void>

    @Test
    fun twoOperationsRunCorrect() {
        givenTwoGridCreations()

        whenRunOnTransaction()

        thenTwoGridWereCreated()
    }

    @Test
    fun oneOperationOfTwoFailsShouldRollback() {
        givenTwoGridCreationsAndSecondWithError()

        whenRunOnTransaction()

        thenAnyGridWasCreated()
    }

    private fun givenTwoGridCreationsAndSecondWithError() {
        operation = {
            gridRepository.put(gridOne)
                    .concatWith(Completable.error(RuntimeException()))
                    .blockingAwait()
        }
    }

    private fun givenTwoGridCreations() {
        operation = {
            gridRepository.put(gridOne)
                    .concatWith(gridRepository.put(gridTwo))
                    .blockingAwait()
        }
    }

    private fun whenRunOnTransaction() {
        transactionTestObserver = transactionService.runOnTransaction(operation).test().await()
    }

    private fun thenTwoGridWereCreated() {
        val gridOneFound = gridRepository.find(gridOne.id).blockingGet()
        val gridTwoFound = gridRepository.find(gridTwo.id).blockingGet()
        transactionTestObserver.assertComplete()
        then(gridOneFound).isNotNull
        then(gridTwoFound).isNotNull
    }

    private fun thenAnyGridWasCreated() {
        val gridOneFound = gridRepository.find(gridOne.id).blockingGet()
        val gridTwoFound = gridRepository.find(gridTwo.id).blockingGet()
        transactionTestObserver.assertNotComplete()
        then(gridOneFound).isNull()
        then(gridTwoFound).isNull()
    }
}