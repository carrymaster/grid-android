package com.gocarry.grid.room.service

import org.assertj.core.api.BDDAssertions.then
import org.junit.Test

class RoomGridIdServiceTest {

    private lateinit var idList: MutableList<String>
    private val roomIdGenerator = RoomIdGenerator()

    @Test
    fun `generate twice should return diferents ids`() {
        whenGenerateTwice()
        thenIdsAreUnique()
    }

    private fun whenGenerateTwice() {
        idList = mutableListOf()
        idList.add(roomIdGenerator.generate())
        idList.add(roomIdGenerator.generate())
    }

    private fun thenIdsAreUnique() {
        then(idList.distinct().size).isEqualTo(2)
    }
}