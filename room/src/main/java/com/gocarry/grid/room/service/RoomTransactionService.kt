package com.gocarry.grid.room.service

import com.gocarry.grid.core.domain.service.TransactionService
import com.gocarry.grid.room.database.GridRoomDatabase
import io.reactivex.Completable

internal class RoomTransactionService(private val gridRoomDatabase: GridRoomDatabase) : TransactionService {
    override fun runOnTransaction(operation: () -> Unit): Completable {
        return Completable.create { emitter ->
            gridRoomDatabase.runInTransaction {
                operation()
                emitter.onComplete()
            }
        }
    }

}