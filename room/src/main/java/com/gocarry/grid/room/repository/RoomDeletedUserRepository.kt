package com.gocarry.grid.room.repository

import com.gocarry.grid.core.domain.DeletedUser
import com.gocarry.grid.core.domain.repository.DeletedUserRepository
import com.gocarry.grid.room.database.GridRoomDatabase
import com.gocarry.grid.room.entity.DeletedUserEntity
import io.reactivex.Completable
import io.reactivex.Maybe

internal class RoomDeletedUserRepository(private val gridRoomDatabase: GridRoomDatabase) : DeletedUserRepository {
    override fun find(userId: String, gridId: String): Maybe<DeletedUser> {
        return gridRoomDatabase.deletedUserDao()
                .find(userId, gridId)
                .map { DeletedUser(it.id, it.gridId) }
    }

    override fun put(deleteUser: DeletedUser): Completable {
        return Completable.fromAction {
            gridRoomDatabase.deletedUserDao()
                    .add(createDeletedUserEntity(deleteUser))
        }
    }

    override fun removeByGrid(gridId: String): Completable {
        return Completable.fromAction {
            gridRoomDatabase.deletedUserDao()
                    .removeByGridId(gridId)
        }
    }

    private fun createDeletedUserEntity(deleteUser: DeletedUser) =
            deleteUser.let { DeletedUserEntity(it.id, it.gridId) }

}