package com.gocarry.grid.room.database.converters

import android.arch.persistence.room.TypeConverter
import com.gocarry.grid.room.entity.BalanceEntity
import com.google.gson.Gson

internal class BalanceListConverter {
    private val gson = Gson()

    @TypeConverter
    fun setToJson(value: List<BalanceEntity>?): String {
        return gson.toJson(value)
    }

    @TypeConverter
    fun jsonToSet(value: String): List<BalanceEntity>? {
        val objects = gson.fromJson(value, Array<BalanceEntity>::class.java)
        return objects.toList()
    }
}