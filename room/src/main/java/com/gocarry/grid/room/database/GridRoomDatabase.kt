package com.gocarry.grid.room.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.gocarry.grid.room.dao.DeleteUserEntityDao
import com.gocarry.grid.room.dao.ExpenseEntityDao
import com.gocarry.grid.room.dao.GridEntityDao
import com.gocarry.grid.room.dao.GridUserEntityDao
import com.gocarry.grid.room.database.converters.BalanceListConverter
import com.gocarry.grid.room.database.converters.PaymentListConverter
import com.gocarry.grid.room.entity.DeletedUserEntity
import com.gocarry.grid.room.entity.ExpenseEntity
import com.gocarry.grid.room.entity.GridEntity
import com.gocarry.grid.room.entity.GridUserEntity

@Database(entities = [GridEntity::class, GridUserEntity::class, ExpenseEntity::class, DeletedUserEntity::class], version = 1)
@TypeConverters(BalanceListConverter::class, PaymentListConverter::class)
internal abstract class GridRoomDatabase : RoomDatabase() {

    abstract fun gridDao(): GridEntityDao

    abstract fun gridUserDao(): GridUserEntityDao

    abstract fun expenseDao(): ExpenseEntityDao

    abstract fun deletedUserDao(): DeleteUserEntityDao
}