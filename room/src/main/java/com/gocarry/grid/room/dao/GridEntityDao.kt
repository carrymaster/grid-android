package com.gocarry.grid.room.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.gocarry.grid.room.entity.GridEntity
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
internal interface GridEntityDao {

    @Query("SELECT * FROM grid")
    fun findAll(): Single<List<GridEntity>>

    @Query("SELECT * FROM grid WHERE id = :id ")
    fun findGrid(id: String): Maybe<GridEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addGrid(gridEntity: GridEntity)

    @Query("DELETE FROM grid WHERE id = :id")
    fun deleteGrid(id: String)
}