package com.gocarry.grid.room.database.converters

import android.arch.persistence.room.TypeConverter
import com.gocarry.grid.room.entity.ExpenseEntity
import com.google.gson.Gson

internal class PaymentListConverter {
    private val gson = Gson()

    @TypeConverter
    fun setToJson(value: List<ExpenseEntity.Payment>?): String {
        return gson.toJson(value)
    }

    @TypeConverter
    fun jsonToSet(value: String): List<ExpenseEntity.Payment>? {
        val objects = gson.fromJson(value, Array<ExpenseEntity.Payment>::class.java)
        return objects.toList()
    }
}

