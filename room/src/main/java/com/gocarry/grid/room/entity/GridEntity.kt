package com.gocarry.grid.room.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "grid")
data class GridEntity(@ColumnInfo(name = "id") @PrimaryKey var id: String = "",
                      @ColumnInfo(name = "description") var description: String = "",
                      @ColumnInfo(name = "user_count") var userCount: Int = 0,
                      @ColumnInfo(name = "balance_list") var balanceList: List<BalanceEntity> = emptyList())

data class BalanceEntity(var userBalanceList: List<UserBalanceEntity> = emptyList(), var amount: Float)

data class UserBalanceEntity(var userId: String, var amount: Float)