package com.gocarry.grid.room.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "expense",
        foreignKeys = [
            ForeignKey(entity = GridEntity::class,
                    parentColumns = ["id"],
                    childColumns = ["grid_id"]
            )])
internal data class ExpenseEntity(@ColumnInfo(name = "id") @PrimaryKey var id: String = "",
                                  @ColumnInfo(name = "grid_id") var gridId: String = "",
                                  @ColumnInfo(name = "description") var description: String = "",
                                  @ColumnInfo(name = "payments") var payments: List<Payment> = listOf()) {

    internal data class Payment(var userId: String = "",
                                var amount: Float = 0f)
}