package com.gocarry.grid.room.config

import android.arch.persistence.room.Room
import android.content.Context
import com.gocarry.grid.core.action.AddGridUser
import com.gocarry.grid.core.action.CreateGrid
import com.gocarry.grid.core.action.FindGridDetail
import com.gocarry.grid.core.action.FindGrids
import com.gocarry.grid.core.domain.service.AddGridUserPolicy
import com.gocarry.grid.core.domain.service.GridExistenceValidator
import com.gocarry.grid.core.domain.service.GridToCreateValidator
import com.gocarry.grid.room.database.GridRoomDatabase
import com.gocarry.grid.room.repository.RoomGridRepository
import com.gocarry.grid.room.repository.RoomGridUserRepository
import com.gocarry.grid.room.service.RoomIdGenerator
import com.gocarry.grid.room.service.RoomTransactionService
import com.gocarry.grid.utils.InstanceCache


object RoomModule {

    val roomFindGrids by lazy { FindGrids(gridRepository) }

    val roomFindGridDetail by lazy { FindGridDetail(gridExistenceValidator, gridUserRepository) }

    val roomCreateGrid by lazy { CreateGrid(gridRepository, idGenerator, gridUserRepository, gridToCreateValidator, transactionService) }

    val roomAddGridUser by lazy { AddGridUser(gridUserRepository, addGridUserPolicy, gridRepository, transactionService) }

    private val idGenerator
        get() = InstanceCache.instance(RoomIdGenerator::class) { RoomIdGenerator() }

    private val gridRepository
        get() = InstanceCache.instance(RoomGridRepository::class) { RoomGridRepository(gridRoomDatabase) }

    private val gridUserRepository
        get() = InstanceCache.instance(RoomGridUserRepository::class) { RoomGridUserRepository(gridRoomDatabase) }

    private val addGridUserPolicy
        get() = InstanceCache.instance(AddGridUserPolicy::class) { AddGridUserPolicy(gridExistenceValidator) }

    private val gridToCreateValidator
        get() = InstanceCache.instance(GridToCreateValidator::class) { GridToCreateValidator() }

    private val gridExistenceValidator
        get() = InstanceCache.instance(GridExistenceValidator::class) { GridExistenceValidator(gridRepository) }

    private val transactionService
        get() = InstanceCache.instance(RoomTransactionService::class) { RoomTransactionService(gridRoomDatabase) }

    private val gridRoomDatabase
        get() = InstanceCache.instance(GridRoomDatabase::class) {
            Room.databaseBuilder(context, GridRoomDatabase::class.java, "grid-room-database")
                    .fallbackToDestructiveMigration()
                    .build()
        }

    private val context
        get() = InstanceCache.get<Context>(Context::class)
}