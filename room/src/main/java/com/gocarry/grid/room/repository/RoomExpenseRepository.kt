package com.gocarry.grid.room.repository

import com.gocarry.grid.core.domain.Expense
import com.gocarry.grid.core.domain.Payment
import com.gocarry.grid.core.domain.repository.ExpenseRepository
import com.gocarry.grid.room.database.GridRoomDatabase
import com.gocarry.grid.room.entity.ExpenseEntity
import io.reactivex.Completable
import io.reactivex.Maybe

internal class RoomExpenseRepository(private val gridRoomDatabase: GridRoomDatabase) : ExpenseRepository {
    private val expenseEntityDao get() = gridRoomDatabase.expenseDao()

    override fun find(id: String): Maybe<Expense> {
        return expenseEntityDao.findExpense(id)
                .map { createExpense(it) }
    }

    override fun put(expense: Expense): Completable {
        return Completable.fromAction {
            val expenseEntity = createExpenseEntity(expense)
            expenseEntityDao.addExpense(expenseEntity)
        }
    }

    override fun update(expense: Expense): Completable {
        return Completable.fromAction {
            val expenseEntity = createExpenseEntity(expense)
            expenseEntityDao.updateExpense(expenseEntity)
        }
    }

    override fun remove(id: String): Completable {
        return Completable.fromAction { expenseEntityDao.deleteExpense(id) }
    }

    override fun removeByGrid(gridId: String): Completable {
        return Completable.fromAction { expenseEntityDao.deleteExpensenInGrid(gridId) }
    }

    private fun createExpense(expenseEntity: ExpenseEntity) =
            with(expenseEntity) {
                Expense.create(id, gridId, createPayments(payments), expenseEntity.description)
            }

    private fun createPayments(payments: List<ExpenseEntity.Payment>) =
            payments.map { Payment.create(it.userId, it.amount) }

    private fun createExpenseEntity(expense: Expense) =
            with(expense) {
                ExpenseEntity(id, gridId, description, createEntityPayments(expense.payments))
            }

    private fun createEntityPayments(payments: List<Payment>) =
            payments.map { ExpenseEntity.Payment(it.userId, it.amount) }
}