package com.gocarry.grid.room.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.gocarry.grid.room.entity.GridUserEntity
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface GridUserEntityDao {

    @Query("SELECT * FROM grid_user where grid_id = :gridId")
    fun findByGridId(gridId: String): Single<List<GridUserEntity>>

    @Query("SELECT * FROM grid_user WHERE id = :id AND grid_id = :gridId")
    fun findGridUser(id: String, gridId: String): Maybe<GridUserEntity>

    @Insert
    fun addGridUser(gridUserEntity: GridUserEntity)

    @Insert
    fun addGridUsers(gridUserEntityList: List<GridUserEntity>)

    @Query("DELETE FROM grid_user WHERE id = :userId AND grid_id = :gridId")
    fun remove(userId: String, gridId: String)

    @Query("DELETE FROM grid_user WHERE grid_id = :gridId")
    fun removeByGridId(gridId: String)
}