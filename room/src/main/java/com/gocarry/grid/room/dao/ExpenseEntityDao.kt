package com.gocarry.grid.room.dao

import android.arch.persistence.room.*
import com.gocarry.grid.room.entity.ExpenseEntity
import io.reactivex.Maybe

@Dao
internal interface ExpenseEntityDao {

    @Query("SELECT * FROM expense WHERE id = :id ")
    fun findExpense(id: String): Maybe<ExpenseEntity>

    @Insert
    fun addExpense(expenseEntity: ExpenseEntity)

    @Query("DELETE FROM expense WHERE id = :id")
    fun deleteExpense(id: String)

    @Query("DELETE FROM expense WHERE grid_id = :gridId")
    fun deleteExpensenInGrid(gridId: String)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateExpense(expenseEntity: ExpenseEntity)
}