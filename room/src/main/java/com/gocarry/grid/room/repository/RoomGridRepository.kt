package com.gocarry.grid.room.repository

import com.gocarry.grid.core.domain.Balance
import com.gocarry.grid.core.domain.Grid
import com.gocarry.grid.core.domain.UserBalance
import com.gocarry.grid.core.domain.repository.GridRepository
import com.gocarry.grid.room.database.GridRoomDatabase
import com.gocarry.grid.room.entity.BalanceEntity
import com.gocarry.grid.room.entity.GridEntity
import com.gocarry.grid.room.entity.UserBalanceEntity
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

internal class RoomGridRepository(private val gridRoomDatabase: GridRoomDatabase) : GridRepository {

    private val gridDao get() = gridRoomDatabase.gridDao()

    override fun put(grid: Grid): Completable {
        return Completable.fromAction {
            gridRoomDatabase.gridDao()
                    .addGrid(createGridEntity(grid))
        }
    }

    override fun findAll(): Single<List<Grid>> {
        return gridDao.findAll()
                .map { grids -> grids.map { createGrid(it) } }
    }

    override fun find(id: String): Maybe<Grid> {
        return gridRoomDatabase.gridDao()
                .findGrid(id)
                .map { createGrid(it) }
    }

    override fun remove(id: String): Completable {
        return Completable.fromAction {
            gridRoomDatabase.gridDao()
                    .deleteGrid(id)
        }
    }

    private fun createGridEntity(grid: Grid) =
            with(grid) {
                GridEntity(id, description, userCount, createBalanceList(balanceSet))
            }

    private fun createBalanceList(balanceSet: Set<Balance>) =
            balanceSet.map { BalanceEntity(createUserBalanceList(it.userBalances), it.amount) }

    private fun createUserBalanceList(userBalanceSet: Set<UserBalance>) =
            userBalanceSet.map { UserBalanceEntity(it.userId, it.balanceAmount) }

    private fun createGrid(gridEntity: GridEntity): Grid {
        return with(gridEntity) {
            Grid.create(id, description, userCount, createBalanceSet(balanceList))
        }
    }

    private fun createBalanceSet(balanceList: List<BalanceEntity>): MutableSet<Balance> {
        return balanceList.map { Balance.create(createUserBalanceSet(it.userBalanceList), it.amount) }.toMutableSet()
    }

    private fun createUserBalanceSet(userBalanceList: List<UserBalanceEntity>): Set<UserBalance> {
        return userBalanceList.map { UserBalance(it.userId, it.amount) }.toSet()
    }
}