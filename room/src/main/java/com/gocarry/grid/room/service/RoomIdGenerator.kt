package com.gocarry.grid.room.service

import com.gocarry.grid.core.domain.service.IdGenerator
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

class RoomIdGenerator : IdGenerator {
    override fun generate(): String {
        return DateTimeFormat.forPattern("HHmmssSSS")
                .print(DateTime.now())
    }
}