package com.gocarry.grid.room.repository

import com.gocarry.grid.core.domain.GridUser
import com.gocarry.grid.core.domain.repository.GridUserRepository
import com.gocarry.grid.room.database.GridRoomDatabase
import com.gocarry.grid.room.entity.GridUserEntity
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

internal class RoomGridUserRepository(private val gridRoomDatabase: GridRoomDatabase) : GridUserRepository {
    private val gridUserDao get() = gridRoomDatabase.gridUserDao()

    override fun find(id: String, gridId: String): Maybe<GridUser> {
        return gridUserDao.findGridUser(id, gridId)
                .map { createGridUser(it) }
    }

    override fun findAllByGrid(gridId: String): Single<List<GridUser>> {
        return gridUserDao.findByGridId(gridId)
                .map { users -> users.map { createGridUser(it) } }
    }

    override fun put(gridUser: GridUser): Completable {
        return Completable.fromAction {
            val gridUserEntity = createUserEntity(gridUser)
            gridUserDao.addGridUser(gridUserEntity)
        }
    }

    override fun putAll(gridUserList: List<GridUser>): Completable {
        return Completable.fromAction {
            val gridUserEntityList = gridUserList.map { createUserEntity(it) }
            gridUserDao.addGridUsers(gridUserEntityList)
        }
    }

    override fun remove(id: String, gridId: String): Completable {
        return Completable.fromAction { gridUserDao.remove(id, gridId) }
    }

    override fun removeByGrid(gridId: String): Completable {
        return Completable.fromAction { gridUserDao.removeByGridId(gridId) }
    }

    private fun createUserEntity(gridUser: GridUser) =
            GridUserEntity(gridUser.id, gridUser.gridId, gridUser.userName)

    private fun createGridUser(gridUserEntity: GridUserEntity) =
            GridUser.create(gridUserEntity.id, gridUserEntity.userName, gridUserEntity.gridId)
}
