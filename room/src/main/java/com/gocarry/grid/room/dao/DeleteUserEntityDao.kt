package com.gocarry.grid.room.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.gocarry.grid.room.entity.DeletedUserEntity
import io.reactivex.Maybe

@Dao
interface DeleteUserEntityDao {

    @Insert
    fun add(deletedUserEntity: DeletedUserEntity)

    @Query("SELECT * FROM deleted_grid_user WHERE id = :userId AND grid_id = :gridId")
    fun find(userId: String, gridId: String): Maybe<DeletedUserEntity>

    @Query("DELETE FROM deleted_grid_user WHERE grid_id = :gridId")
    fun removeByGridId(gridId: String)

}