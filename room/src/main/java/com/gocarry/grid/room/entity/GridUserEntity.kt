package com.gocarry.grid.room.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey

@Entity(tableName = "grid_user", primaryKeys = ["id", "grid_id"],
        foreignKeys = [
            ForeignKey(entity = GridEntity::class,
                    parentColumns = ["id"],
                    childColumns = ["grid_id"]
            )])
data class GridUserEntity(@ColumnInfo(name = "id") var id: String = "",
                          @ColumnInfo(name = "grid_id") var gridId: String = "",
                          @ColumnInfo(name = "user_name") var userName: String = "")