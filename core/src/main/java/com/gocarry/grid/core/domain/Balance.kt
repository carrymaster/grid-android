package com.gocarry.grid.core.domain

class Balance private constructor(userBalances: Set<UserBalance>, amount: Float) {

    val userIds = userBalances.map { it.userId }.toSet()

    var userBalances: Set<UserBalance> = userBalances
        private set

    var amount: Float = amount
        private set

    fun update(balance: Balance) {
        validateBalance(balance)
        updateAmount(balance)
        updateUserBalanceSet(balance)
    }

    fun isBalanceOf(userIds: Set<String>): Boolean {
        return this.userIds.containsAll(userIds)
    }

    private fun validateBalance(balance: Balance) {
        if (!isBalanceOf(balance.userIds)) throw InvalidBalanceUpdate(userIds, balance.userIds)
    }

    private fun updateAmount(expenseBalance: Balance) {
        amount += expenseBalance.amount
    }

    private fun updateUserBalanceSet(expenseBalance: Balance) {
        userBalances = userBalances.map { it.copy(balanceAmount = it.balanceAmount + expenseBalance.userBalanceAmountOf(it.userId)!!) }.toSet()
    }

    private fun userBalanceAmountOf(userId: String): Float? {
        return userBalances.find { it.userId == userId }?.balanceAmount
    }

    override fun equals(other: Any?): Boolean {
        return other != null && other is Balance
                && isBalanceOf(other.userIds)
    }

    override fun hashCode(): Int {
        return userIds.toString().hashCode()
    }

    companion object {
        fun create(userBalanceSet: Set<UserBalance>, amount: Float): Balance {
            return Balance(userBalanceSet, amount)
        }

        fun createInitial(userIdSet: Set<String>): Balance {
            val userBalanceSet = userIdSet.map { UserBalance(it, 0f) }.toSet()
            return Balance(userBalanceSet, 0f)
        }
    }
}

data class UserBalance(val userId: String, val balanceAmount: Float)

class InvalidBalanceUpdate(userIdSet: Set<String>, userIdSetExpected: Set<String>) : RuntimeException("Balance users expected $userIdSet but was $userIdSetExpected")