package com.gocarry.grid.core.domain

data class GridUser(val id: String, val userName: String, val gridId: String) {
    companion object {
        fun create(id: String, userName: String, gridId: String): GridUser {
            return GridUser(id, userName, gridId)
        }
    }
}