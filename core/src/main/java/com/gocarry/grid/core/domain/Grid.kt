package com.gocarry.grid.core.domain

import com.gocarry.grid.core.domain.service.BalanceCalculator

class Grid private constructor(val id: String,
                               val description: String,
                               userCount: Int,
                               private val balances: MutableSet<Balance>) { //TODO: No exponer mutableSet

    val balanceSet get() = balances.toSet()
    val totalSpent get() = balances.map { it.amount }.sum()
    var userCount = userCount
        private set

    fun add(expense: Expense) {
        val balance = findOrCreateBalance(expense.userIds)
        val balanceCalculator = BalanceCalculator()
        balance.update(balanceCalculator.calculate(expense))
    }

    fun remove(expense: Expense) {
        val balance = findBalance(expense.userIds)
                ?: throw BalanceNotExistException(id, expense.userIds)
        val inverseBalance = BalanceCalculator().calculateInverse(expense)
        balance.update(inverseBalance)
        cleanBalanceSet()
    }

    fun incrementUserCount() {
        userCount++
    }

    fun decrementUserCount() {
        if (userCount > 0) {
            userCount--
        }
    }

    fun findBalance(userSet: Set<String>): Balance? {
        return balances.find { it.isBalanceOf(userSet) }
    }

    private fun cleanBalanceSet() {
        balances.removeAll { it.amount == 0f }
    }

    private fun findOrCreateBalance(userIdSet: Set<String>) =
            findBalance(userIdSet)
                    ?: Balance.createInitial(userIdSet).also { balances.add(it) }

    override fun equals(other: Any?): Boolean {
        return other != null
                && other is Grid
                && id == other.id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    companion object {
        fun create(id: String, description: String, userCount: Int, balanceSet: MutableSet<Balance>): Grid {
            return Grid(id, description, userCount, balanceSet)
        }

        fun createInitial(id: String, description: String, userCount: Int = 0): Grid {
            return Grid(id, description, userCount, mutableSetOf())
        }
    }
}

class BalanceNotExistException(gridId: String, userIdSet: Set<String>) : RuntimeException("Balance of $userIdSet not exist in grid $gridId")