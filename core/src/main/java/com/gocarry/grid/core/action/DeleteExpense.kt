package com.gocarry.grid.core.action

import com.gocarry.grid.core.domain.Expense
import com.gocarry.grid.core.domain.Grid
import com.gocarry.grid.core.domain.policy.RemoveExpensePolicy
import com.gocarry.grid.core.domain.repository.ExpenseRepository
import com.gocarry.grid.core.domain.repository.GridRepository
import com.gocarry.grid.core.domain.service.TransactionService
import io.reactivex.Completable

class DeleteExpense(private val gridRepository: GridRepository,
                    private val expenseRepository: ExpenseRepository,
                    private val removeExpensePolicy: RemoveExpensePolicy,
                    private val transactionService: TransactionService) {

    operator fun invoke(id: String): Completable {
        return removeExpensePolicy.apply(id)
                .andThen(expenseRepository.find(id))
                .flatMapCompletable { removeExpenseFromGrid(it) }
    }

    private fun removeExpenseFromGrid(expense: Expense): Completable? {
        return gridRepository.find(expense.gridId)
                .doOnSuccess { it.remove(expense) }
                .flatMapCompletable { updateGridAndRemoveExpense(expense, it) }
    }

    private fun updateGridAndRemoveExpense(expense: Expense, it: Grid): Completable {
        return transactionService.runOnTransaction {
            expenseRepository.remove(expense.id)
                    .concatWith(gridRepository.put(it))
                    .blockingAwait()
        }
    }
}