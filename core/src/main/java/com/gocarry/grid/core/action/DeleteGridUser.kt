package com.gocarry.grid.core.action

import com.gocarry.grid.core.domain.DeletedUser
import com.gocarry.grid.core.domain.Grid
import com.gocarry.grid.core.domain.repository.DeletedUserRepository
import com.gocarry.grid.core.domain.repository.GridRepository
import com.gocarry.grid.core.domain.repository.GridUserRepository
import com.gocarry.grid.core.domain.service.GridUserExistenceValidator
import com.gocarry.grid.core.domain.service.TransactionService
import io.reactivex.Completable

class DeleteGridUser(private val gridUserRepository: GridUserRepository,
                     private val deleteUserRepository: DeletedUserRepository,
                     private val transactionService: TransactionService,
                     private val gridUserExistenceValidator: GridUserExistenceValidator,
                     private val gridRepository: GridRepository) {

    operator fun invoke(userToDelete: UserToDelete): Completable {
        return gridUserExistenceValidator.validate(GridUserExistenceValidator.GridUserToValidate(userToDelete.id, userToDelete.gridId))
                .andThen(findGridAndDecrementUserCount(userToDelete.gridId))
                .flatMapCompletable { grid ->
                    transactionService.runOnTransaction {
                        Completable.concatArray(removeGridUser(userToDelete), addDeletedUser(userToDelete), saveGrid(grid)).blockingAwait()
                    }
                }
    }

    private fun findGridAndDecrementUserCount(gridId: String) =
            gridRepository.find(gridId)
                    .doOnSuccess { it.decrementUserCount() }

    private fun saveGrid(grid: Grid): Completable {
        return gridRepository.put(grid)
    }

    private fun removeGridUser(userToDelete: UserToDelete): Completable {
        return gridUserRepository.remove(userToDelete.id, userToDelete.gridId)
    }

    private fun addDeletedUser(userToDelete: UserToDelete): Completable {
        return deleteUserRepository.put(DeletedUser(userToDelete.id, userToDelete.gridId))
    }

    data class UserToDelete(val id: String, val gridId: String)
}