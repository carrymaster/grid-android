package com.gocarry.grid.core.domain.policy

import com.gocarry.grid.core.domain.repository.ExpenseRepository
import com.gocarry.grid.core.domain.service.GridExistenceValidator
import io.reactivex.Completable

class RemoveExpensePolicy(private val gridExistenceValidator: GridExistenceValidator,
                          private val expenseRepository: ExpenseRepository) {
    fun apply(expenseId: String): Completable {
        return expenseRepository.find(expenseId)
                .toSingle()
                .flatMapCompletable { gridExistenceValidator.validate(it.gridId) }
    }
}