package com.gocarry.grid.core.domain.policy

import com.gocarry.grid.core.action.AddExpense
import com.gocarry.grid.core.domain.service.GridExistenceValidator
import com.gocarry.grid.core.domain.service.GridUserExistenceValidator
import com.gocarry.grid.core.domain.service.GridUserExistenceValidator.GridUserToValidate
import io.reactivex.Completable

class AddExpensePolicy(private val gridExistenceValidator: GridExistenceValidator,
                       private val gridUserExistenceValidator: GridUserExistenceValidator) {
    fun apply(expenseToAdd: AddExpense.ExpenseToAdd): Completable {
        return gridExistenceValidator.validate(expenseToAdd.gridId)
                .andThen(validatePaymentUsers(expenseToAdd))
    }

    private fun validatePaymentUsers(expenseToAdd: AddExpense.ExpenseToAdd): Completable {
        val gridUserValidations = expenseToAdd.paymentList
                .map { GridUserToValidate(it.userId, expenseToAdd.gridId) }
                .map { gridUserExistenceValidator.validate(it) }
        return Completable.concat(gridUserValidations)
    }
}