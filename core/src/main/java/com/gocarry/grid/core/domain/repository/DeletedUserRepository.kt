package com.gocarry.grid.core.domain.repository

import com.gocarry.grid.core.domain.DeletedUser
import io.reactivex.Completable
import io.reactivex.Maybe

interface DeletedUserRepository {
    fun find(userId: String, gridId: String): Maybe<DeletedUser>

    fun put(deleteUser: DeletedUser): Completable

    fun removeByGrid(gridId: String): Completable
}