package com.gocarry.grid.core.domain.service

interface IdGenerator {
    fun generate(): String
}