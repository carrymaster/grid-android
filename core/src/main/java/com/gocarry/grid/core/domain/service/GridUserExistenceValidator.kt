package com.gocarry.grid.core.domain.service

import com.gocarry.grid.core.domain.GridUser
import com.gocarry.grid.core.domain.repository.GridUserRepository
import io.reactivex.Completable
import io.reactivex.Maybe

class GridUserExistenceValidator(private val gridUserRepository: GridUserRepository) {
    fun validate(gridUserToValidate: GridUserToValidate): Completable {
        return with(gridUserToValidate) {
            gridUserRepository.find(id, gridId)
                    .switchIfEmpty(createGridUserNotExistException(gridUserToValidate))
                    .ignoreElement()
        }
    }

    private fun createGridUserNotExistException(gridUserToValidate: GridUserToValidate): Maybe<GridUser> =
            with(gridUserToValidate) {
                Maybe.error(GridUserNotExistException(id, gridId))
            }

    data class GridUserToValidate(val id: String, val gridId: String)
}

class GridUserNotExistException(gridUserId: String, gridId: String) : RuntimeException("User $gridUserId does not belong to grid $gridId")