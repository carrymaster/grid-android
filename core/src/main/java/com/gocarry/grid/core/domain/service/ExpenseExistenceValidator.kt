package com.gocarry.grid.core.domain.service

import com.gocarry.grid.core.domain.repository.ExpenseRepository
import io.reactivex.Completable
import io.reactivex.Maybe

class ExpenseExistenceValidator(private val expenseRepository: ExpenseRepository) {
    fun validate(expenseId: String): Completable {
        return expenseRepository.find(expenseId)
                .switchIfEmpty(Maybe.error(ExpenseNotExistException(expenseId)))
                .ignoreElement()
    }
}

class ExpenseNotExistException(expenseId: String) : RuntimeException("Expense $expenseId not exist")