package com.gocarry.grid.core.action

import com.gocarry.grid.core.domain.Expense
import com.gocarry.grid.core.domain.Grid
import com.gocarry.grid.core.domain.Payment
import com.gocarry.grid.core.domain.policy.AddExpensePolicy
import com.gocarry.grid.core.domain.repository.ExpenseRepository
import com.gocarry.grid.core.domain.repository.GridRepository
import com.gocarry.grid.core.domain.service.IdGenerator
import com.gocarry.grid.core.domain.service.TransactionService
import io.reactivex.Completable


class AddExpense(private val transactionService: TransactionService,
                 private val idGenerator: IdGenerator,
                 private val addExpensePolicy: AddExpensePolicy,
                 private val gridRepository: GridRepository,
                 private val expenseRepository: ExpenseRepository) {

    operator fun invoke(expenseToAdd: ExpenseToAdd): Completable {
        return addExpensePolicy.apply(expenseToAdd)
                .andThen(gridRepository.find(expenseToAdd.gridId))
                .flatMapCompletable { addExpenseToGrid(it, expenseToAdd) }
    }

    private fun addExpenseToGrid(grid: Grid, expenseToAdd: ExpenseToAdd): Completable {
        val expense = createExpense(expenseToAdd).also { grid.add(it) }
        return transactionService.runOnTransaction {
            expenseRepository.put(expense)
                    .concatWith(gridRepository.put(grid))
                    .blockingAwait()
        }
    }

    private fun createExpense(expenseToAdd: ExpenseToAdd) =
            with(expenseToAdd) {
                Expense.create(idGenerator.generate(), expenseToAdd.gridId, createPaymentList(paymentList), description)
            }

    private fun createPaymentList(paymentList: List<ExpenseToAdd.Payment>) =
            paymentList.map { Payment.create(it.userId, it.amount) }

    data class ExpenseToAdd(val gridId: String, val paymentList: List<Payment>, val description: String) {
        data class Payment(val userId: String, val amount: Float)
    }
}

