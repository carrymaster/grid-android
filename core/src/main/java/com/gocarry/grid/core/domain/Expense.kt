package com.gocarry.grid.core.domain

data class Expense(val id: String, val gridId: String, val payments: List<Payment>, val description: String) {
    val amount = payments.map { it.amount }.sum()
    val userIds: Set<String> = payments.map { it.userId }.toSet()

    companion object {
        fun create(id: String, gridId: String, paymentList: List<Payment>, description: String): Expense {
            return Expense(id, gridId, paymentList, description)
        }
    }
}