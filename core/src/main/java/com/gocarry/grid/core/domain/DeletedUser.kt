package com.gocarry.grid.core.domain

data class DeletedUser(val id: String, val gridId: String)