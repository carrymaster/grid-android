package com.gocarry.grid.core.domain

data class Payment(val userId: String, val amount: Float) {
    companion object {
        fun create(userId: String, amount: Float): Payment {
            return Payment(userId, amount)
        }
    }
}