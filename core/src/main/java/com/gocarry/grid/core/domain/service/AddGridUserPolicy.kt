package com.gocarry.grid.core.domain.service

import com.gocarry.grid.core.action.AddGridUser
import io.reactivex.Completable

class AddGridUserPolicy(private val gridExistenceValidator: GridExistenceValidator) {
    fun apply(gridUserToAdd: AddGridUser.GridUserToAdd): Completable {
        return Completable.fromAction { validateFields(gridUserToAdd) }
                .andThen(gridExistenceValidator.validate(gridUserToAdd.gridId))
    }

    private fun validateFields(gridUserToAdd: AddGridUser.GridUserToAdd) {
        require(gridUserToAdd.userId.isNotEmpty()) { "user-id cannot be empty" }
        require(gridUserToAdd.userName.isNotEmpty()) { "user-name cannot be empty" }
    }
}