package com.gocarry.grid.core.action

import com.gocarry.grid.core.domain.GridUser
import com.gocarry.grid.core.domain.repository.GridUserRepository
import com.gocarry.grid.core.domain.service.GridExistenceValidator
import io.reactivex.Single

class FindGridDetail(private val gridExistenceValidator: GridExistenceValidator,
                     private val gridUserRepository: GridUserRepository) {

    operator fun invoke(gridId: String): Single<GridDetailResponse> {
        return validate(gridId)
                .andThen(findGridUsers(gridId))
                .map { createResponses(it) }
                .map { GridDetailResponse(it) }
    }

    private fun validate(gridId: String) =
            gridExistenceValidator.validate(gridId)

    private fun findGridUsers(gridId: String) =
            gridUserRepository.findAllByGrid(gridId)

    private fun createResponses(it: List<GridUser>) =
            it.map { GridUserResponse(it.id, it.userName) }

    data class GridDetailResponse(val users: List<GridUserResponse>)

    data class GridUserResponse(val id: String, val userName: String)
}