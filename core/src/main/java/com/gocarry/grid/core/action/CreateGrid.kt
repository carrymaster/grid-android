package com.gocarry.grid.core.action

import com.gocarry.grid.core.domain.Grid
import com.gocarry.grid.core.domain.GridUser
import com.gocarry.grid.core.domain.repository.GridRepository
import com.gocarry.grid.core.domain.repository.GridUserRepository
import com.gocarry.grid.core.domain.service.GridToCreateValidator
import com.gocarry.grid.core.domain.service.IdGenerator
import com.gocarry.grid.core.domain.service.TransactionService
import io.reactivex.Completable

class CreateGrid(private val gridRepository: GridRepository,
                 private val idGenerator: IdGenerator,
                 private val gridUserRepository: GridUserRepository,
                 private val gridToCreateValidator: GridToCreateValidator,
                 private val transactionService: TransactionService) {

    operator fun invoke(gridToCreate: GridToCreate): Completable {
        return gridToCreateValidator.validate(gridToCreate)
                .toSingleDefault(createGrid(gridToCreate))
                .flatMapCompletable { grid -> saveGridAndUser(grid, gridToCreate) }
    }

    private fun saveGridAndUser(grid: Grid, gridToCreate: GridToCreate): Completable {
        return transactionService.runOnTransaction {
            gridRepository.put(grid).blockingAwait()
            gridUserRepository.put(gridUserList(gridToCreate, grid)).blockingAwait()
        }
    }

    private fun createGrid(gridToCreate: GridToCreate) =
            Grid.createInitial(idGenerator.generate(), gridToCreate.description, 1)

    private fun gridUserList(gridToCreate: GridToCreate, grid: Grid) =
            gridToCreate.creator.let { GridUser.create(it.id, it.userName, grid.id) }

    data class GridToCreate(val description: String, val creator: Creator) {
        data class Creator(val id: String, val userName:String)
    }
}