package com.gocarry.grid.core.domain.repository

import com.gocarry.grid.core.domain.Grid
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

interface GridRepository {
    fun put(grid: Grid): Completable
    fun find(id: String): Maybe<Grid>
    fun remove(id: String): Completable
    fun findAll(): Single<List<Grid>>
}