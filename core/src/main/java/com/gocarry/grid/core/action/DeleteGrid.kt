package com.gocarry.grid.core.action

import com.gocarry.grid.core.domain.repository.DeletedUserRepository
import com.gocarry.grid.core.domain.repository.ExpenseRepository
import com.gocarry.grid.core.domain.repository.GridRepository
import com.gocarry.grid.core.domain.repository.GridUserRepository
import com.gocarry.grid.core.domain.service.GridExistenceValidator
import com.gocarry.grid.core.domain.service.TransactionService
import io.reactivex.Completable

class DeleteGrid(private val transactionService: TransactionService,
                 private val gridExistenceValidator: GridExistenceValidator,
                 private val gridRepository: GridRepository,
                 private val gridUserRepository: GridUserRepository,
                 private val expenseRepository: ExpenseRepository,
                 private val deletedGridUserRepository: DeletedUserRepository) {
    
    operator fun invoke(gridId: String): Completable {
        return gridExistenceValidator.validate(gridId)
                .andThen(removeGrid(gridId))
    }

    private fun removeGrid(gridId: String): Completable {
        return transactionService.runOnTransaction {
            expenseRepository.removeByGrid(gridId).blockingAwait()
            gridUserRepository.removeByGrid(gridId).blockingAwait()
            deletedGridUserRepository.removeByGrid(gridId).blockingGet()
            gridRepository.remove(gridId).blockingAwait()
        }
    }
}