package com.gocarry.grid.core.action

import com.gocarry.grid.core.domain.Grid
import com.gocarry.grid.core.domain.repository.GridRepository
import io.reactivex.Single

class FindGrids(private val gridRepository: GridRepository) {

    operator fun invoke(): Single<List<GridResponse>> {
        return gridRepository.findAll()
                .map { createResponse(it) }
    }

    private fun createResponse(grids: List<Grid>) =
            grids.map { GridResponse(it.id, it.description, it.totalSpent, it.userCount) }

    data class GridResponse(val id: String, val description: String, val totalSpent: Float, val userCount: Int)
}