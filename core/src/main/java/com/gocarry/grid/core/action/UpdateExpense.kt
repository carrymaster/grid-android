package com.gocarry.grid.core.action

import com.gocarry.grid.core.domain.Expense
import com.gocarry.grid.core.domain.Grid
import com.gocarry.grid.core.domain.Payment
import com.gocarry.grid.core.domain.policy.UpdateExpensePolicy
import com.gocarry.grid.core.domain.repository.ExpenseRepository
import com.gocarry.grid.core.domain.repository.GridRepository
import com.gocarry.grid.core.domain.service.TransactionService
import io.reactivex.Completable
import io.reactivex.rxkotlin.Maybes

class UpdateExpense(private val expenseRepository: ExpenseRepository,
                    private val updateExpensePolicy: UpdateExpensePolicy,
                    private val gridRepository: GridRepository,
                    private val transactionService: TransactionService) {

    operator fun invoke(updatedExpense: UpdatedExpense): Completable {
        return updateExpensePolicy.apply(updatedExpense)
                .andThen(findExpenseAndGrid(updatedExpense))
                .doOnSuccess(updateGrid(updatedExpense))
                .flatMapCompletable(saveExpenseAndGrid(updatedExpense))
    }

    private fun findExpenseAndGrid(updatedExpense: UpdatedExpense) =
            Maybes.zip(expenseRepository.find(updatedExpense.id), gridRepository.find(updatedExpense.gridId))

    private fun updateGrid(updatedExpense: UpdatedExpense): (Pair<Expense, Grid>) -> Unit {
        return { (expense, grid) ->
            val newExpense = createNewExpense(expense, updatedExpense)
            grid.remove(expense)
            grid.add(newExpense)
        }
    }

    private fun saveExpenseAndGrid(updatedExpense: UpdatedExpense): (Pair<Expense, Grid>) -> Completable {
        return { (expense, grid) ->
            transactionService.runOnTransaction {
                expenseRepository.update(createNewExpense(expense, updatedExpense)).blockingAwait()
                gridRepository.put(grid).blockingAwait()
            }
        }
    }

    private fun createNewExpense(expense: Expense, updatedExpense: UpdatedExpense) =
            Expense.create(expense.id, expense.gridId, createPayments(updatedExpense.payments), updatedExpense.description)

    private fun createPayments(payments: List<UpdatedExpense.Payment>): List<Payment> {
        return payments.map { Payment.create(it.userId, it.amount) }
    }

    data class UpdatedExpense(val id: String, val gridId: String, val payments: List<Payment>, val description: String) {
        data class Payment(val userId: String, val amount: Float)
    }
}