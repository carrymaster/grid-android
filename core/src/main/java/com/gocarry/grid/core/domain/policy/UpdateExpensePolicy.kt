package com.gocarry.grid.core.domain.policy

import com.gocarry.grid.core.action.UpdateExpense.UpdatedExpense
import com.gocarry.grid.core.domain.service.ExpenseExistenceValidator
import com.gocarry.grid.core.domain.service.GridUserExistenceValidator
import io.reactivex.Completable

class UpdateExpensePolicy(private val expenseExistenceValidator: ExpenseExistenceValidator,
                          private val gridUserExistenceValidator: GridUserExistenceValidator) {
    fun apply(updatedExpense: UpdatedExpense): Completable {
        val userValidations = updatedExpense.payments.map { GridUserExistenceValidator.GridUserToValidate(it.userId, updatedExpense.gridId) }.map { gridUserExistenceValidator.validate(it) }
        return expenseExistenceValidator.validate(updatedExpense.id)
                .concatWith(Completable.concat(userValidations))
    }
}