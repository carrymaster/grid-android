package com.gocarry.grid.core.domain.repository

import com.gocarry.grid.core.domain.Expense
import io.reactivex.Completable
import io.reactivex.Maybe

interface ExpenseRepository {
    fun put(expense: Expense): Completable
    fun find(id: String): Maybe<Expense>
    fun remove(id: String): Completable
    fun removeByGrid(gridId: String): Completable
    fun update(expense: Expense): Completable
}