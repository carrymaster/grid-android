package com.gocarry.grid.core.domain.service

import io.reactivex.Completable

interface TransactionService {
    fun runOnTransaction(operation: () -> Unit): Completable
}