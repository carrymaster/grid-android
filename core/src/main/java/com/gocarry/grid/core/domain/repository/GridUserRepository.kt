package com.gocarry.grid.core.domain.repository

import com.gocarry.grid.core.domain.GridUser
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

interface GridUserRepository {
    fun put(gridUser: GridUser): Completable
    fun putAll(gridUserList: List<GridUser>): Completable
    fun find(id: String, gridId: String): Maybe<GridUser>
    fun findAllByGrid(gridId: String): Single<List<GridUser>>
    fun removeByGrid(gridId: String): Completable
    fun remove(id: String, gridId: String): Completable
}