package com.gocarry.grid.core.domain.service

import com.gocarry.grid.core.action.CreateGrid
import io.reactivex.Completable

class GridToCreateValidator {
    fun validate(gridToCreate: CreateGrid.GridToCreate): Completable {
        return Completable.fromAction {
            require(gridToCreate.description.isNotEmpty()) { "CreateGrid.GridToCreate.description cannot be empty" }
            require(gridToCreate.creator.id.isNotEmpty()) { "CreateGrid.GridToCreate.Creator.id cannot be empty" }
        }
    }
}