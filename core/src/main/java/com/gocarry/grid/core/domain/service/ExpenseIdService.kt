package com.gocarry.grid.core.domain.service

import io.reactivex.Single

interface ExpenseIdService {
    fun create(): Single<String>
}