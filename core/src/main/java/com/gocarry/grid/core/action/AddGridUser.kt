package com.gocarry.grid.core.action

import com.gocarry.grid.core.domain.GridUser
import com.gocarry.grid.core.domain.repository.GridRepository
import com.gocarry.grid.core.domain.repository.GridUserRepository
import com.gocarry.grid.core.domain.service.AddGridUserPolicy
import com.gocarry.grid.core.domain.service.TransactionService
import io.reactivex.Completable

class AddGridUser(private val gridUserRepository: GridUserRepository,
                  private val addGridUserPolicy: AddGridUserPolicy,
                  private val gridRepository: GridRepository,
                  private val transactionService: TransactionService) {

    operator fun invoke(gridUserToAdd: GridUserToAdd): Completable {
        return addGridUserPolicy.apply(gridUserToAdd)
                .andThen(findGridAndIncrementUserCount(gridUserToAdd))
                .flatMapCompletable { grid ->
                    transactionService.runOnTransaction {
                        val gridUser = createGridUser(gridUserToAdd)
                        gridUserRepository.put(gridUser).blockingAwait()
                        gridRepository.put(grid).blockingAwait()
                    }
                }
    }

    private fun findGridAndIncrementUserCount(gridUserToAdd: GridUserToAdd) =
            gridRepository.find(gridUserToAdd.gridId)
                    .doOnSuccess { it.incrementUserCount() }

    private fun createGridUser(gridUserToAdd: GridUserToAdd) =
            GridUser.create(gridUserToAdd.userId, gridUserToAdd.userName, gridUserToAdd.gridId)

    data class GridUserToAdd(val gridId: String, val userId: String, val userName: String)
}