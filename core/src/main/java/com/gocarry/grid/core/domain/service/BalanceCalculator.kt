package com.gocarry.grid.core.domain.service

import com.gocarry.grid.core.domain.Balance
import com.gocarry.grid.core.domain.Expense
import com.gocarry.grid.core.domain.Payment
import com.gocarry.grid.core.domain.UserBalance

class BalanceCalculator {

    fun calculate(expense: Expense): Balance {
        with(expense) {
            val amountPerUser = amount / payments.size
            return payments.map { UserBalance(it.userId, userBalanceOf(it, amountPerUser)) }
                    .let { Balance.create(it.toSet(), amount) }
        }
    }

    fun calculateInverse(expense: Expense): Balance {
        val balance = calculate(expense)
        return Balance.create(inverseUserBalanceSetOf(balance), inverseAmountOf(balance))
    }

    private fun userBalanceOf(payment: Payment, amountPerUser: Float): Float {
        val absBalance = Math.abs(payment.amount - amountPerUser)
        return when {
            payment.amount > amountPerUser -> absBalance
            payment.amount < amountPerUser -> absBalance * -1f
            else -> 0f
        }
    }

    private fun inverseAmountOf(balance: Balance) = balance.amount * -1

    private fun inverseUserBalanceSetOf(balance: Balance) =
            balance.userBalances.map { it.copy(balanceAmount = it.balanceAmount * -1) }.toSet()

}