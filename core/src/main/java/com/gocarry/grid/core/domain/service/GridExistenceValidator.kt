package com.gocarry.grid.core.domain.service

import com.gocarry.grid.core.domain.repository.GridRepository
import io.reactivex.Completable
import io.reactivex.Maybe

class GridExistenceValidator(private val gridRepository: GridRepository) {
    fun validate(gridId: String): Completable {
        return gridRepository.find(gridId)
                .switchIfEmpty(Maybe.error(GridNotExistException(gridId)))
                .ignoreElement()
    }
}

class GridNotExistException(gridId: String) : RuntimeException("$gridId doesnt exist")