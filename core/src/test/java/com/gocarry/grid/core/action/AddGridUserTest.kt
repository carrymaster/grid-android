package com.gocarry.grid.core.action

import com.gocarry.grid.core.domain.repository.InMemoryGridRepository
import com.gocarry.grid.core.domain.repository.InMemoryGridUserRepository
import com.gocarry.grid.core.domain.service.AddGridUserPolicy
import com.gocarry.grid.core.domain.service.InMemoryTransactionService
import com.gocarry.grid.test.core.domain.factory.GridFactory
import com.gocarry.grid.test.core.domain.factory.GridUserFactory
import com.gocarry.grid.test.extension.anyKotlin
import io.reactivex.Completable
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito

class AddGridUserTest {

    private val initialUserCount = 3
    private val grid = GridFactory.create(userCount = initialUserCount)
    private val gridUser = GridUserFactory.create()

    private val gridRepository = InMemoryGridRepository()
    private val gridUserRepository = InMemoryGridUserRepository()
    private val transactionService = InMemoryTransactionService()
    private val addGridUserPolicy = createAddGridUserPolicy()

    private val addGridUser by lazy { AddGridUser(gridUserRepository, addGridUserPolicy, gridRepository, transactionService) }

    private lateinit var addGridUserObserver: TestObserver<Void>

    @Test
    fun `save grid user`() {
        givenAGrid()

        whenAddGridUser()

        thenGridUserWasAdded()
    }

    @Test
    fun `increment grid user count when user was saved`() {
        givenAGrid()

        whenAddGridUser()

        thenUserCountIsUpdated()
    }

    @Test
    fun `user should not be saved when policy fails`() {
        givenAGrid()
        givenAnFailAddUserPolicy()

        whenAddGridUser()

        thenAddIsFailed()
    }

    private fun givenAnFailAddUserPolicy() {
        given(addGridUserPolicy.apply(anyKotlin())).willReturn(Completable.error(RuntimeException()))
    }

    private fun givenAGrid() {
        gridRepository.put(grid).blockingGet()
    }

    private fun whenAddGridUser() {
        val gridUserToAdd = AddGridUser.GridUserToAdd(gridUser.gridId, gridUser.id, gridUser.userName)
        addGridUserObserver = addGridUser(gridUserToAdd).test()
    }

    private fun thenGridUserWasAdded() {
        val gridUserFound = gridUserRepository.find(gridUser.id, gridUser.gridId).blockingGet()
        addGridUserObserver.assertComplete()
        then(gridUserFound).isEqualTo(gridUser)
    }

    private fun thenUserCountIsUpdated() {
        val gridFound = gridRepository.find(grid.id).blockingGet()
        addGridUserObserver.assertComplete()
        then(gridFound.userCount).isEqualTo(initialUserCount + 1)
    }

    private fun thenAddIsFailed() {
        val gridUserFound = gridUserRepository.find(gridUser.id, gridUser.gridId).blockingGet()
        addGridUserObserver.assertNotComplete()
        then(gridUserFound).isNull()
    }

    private fun createAddGridUserPolicy(): AddGridUserPolicy {
        return Mockito.mock(AddGridUserPolicy::class.java)
                .also { given(it.apply(anyKotlin())).willReturn(Completable.complete()) }
    }
}

