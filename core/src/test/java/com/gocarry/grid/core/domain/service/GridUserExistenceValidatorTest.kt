package com.gocarry.grid.core.domain.service

import com.gocarry.grid.core.domain.repository.InMemoryGridRepository
import com.gocarry.grid.core.domain.repository.InMemoryGridUserRepository
import com.gocarry.grid.core.domain.service.GridUserExistenceValidator.GridUserToValidate
import com.gocarry.grid.test.core.domain.factory.GridFactory
import com.gocarry.grid.test.core.domain.factory.GridUserFactory
import io.reactivex.observers.TestObserver
import org.junit.Test

class GridUserExistenceValidatorTest {

    private val gridId = "gridId"
    private val grid = GridFactory.create(id = gridId)
    private val gridUserId = "gridUserId"
    private val gridUser = GridUserFactory.create(id = gridUserId, gridId = gridId)

    private val gridRepository = InMemoryGridRepository()
    private val gridUserRepository = InMemoryGridUserRepository()

    private val gridUserValidationService by lazy { GridUserExistenceValidator(gridUserRepository) }
    private lateinit var validationObserver: TestObserver<Void>

    @Test
    fun gridUserValid() {
        givenAGrid()
        givenAGridUser()
        whenValidate()
        thenValidationSuccess()
    }

    @Test
    fun gridUserNotExist() {
        whenValidate()
        thenGridUserNotExistExceptionWasThrown()
    }

    private fun givenAGrid() {
        gridRepository.put(grid).blockingGet()
    }

    private fun givenAGridUser() {
        gridUserRepository.put(gridUser).blockingGet()
    }

    private fun whenValidate() {
        val gridUserToValidate = GridUserToValidate(gridUserId, gridId)
        validationObserver = gridUserValidationService.validate(gridUserToValidate).test()
    }

    private fun thenValidationSuccess() {
        validationObserver.assertNoErrors()
        validationObserver.assertComplete()
    }

    private fun thenGridUserNotExistExceptionWasThrown() {
        validationObserver.assertError(GridUserNotExistException::class.java)
    }
}