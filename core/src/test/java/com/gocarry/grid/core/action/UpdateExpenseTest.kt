package com.gocarry.grid.core.action

import com.gocarry.grid.core.domain.policy.UpdateExpensePolicy
import com.gocarry.grid.core.domain.repository.InMemoryExpenseRepository
import com.gocarry.grid.core.domain.repository.InMemoryGridRepository
import com.gocarry.grid.core.domain.service.InMemoryTransactionService
import com.gocarry.grid.test.core.domain.factory.ExpenseFactory
import com.gocarry.grid.test.core.domain.factory.FactoryDefaultValues.gridUserId
import com.gocarry.grid.test.core.domain.factory.GridFactory
import com.gocarry.grid.test.core.domain.factory.PaymentFactory
import com.gocarry.grid.test.extension.anyKotlin
import io.reactivex.Completable
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito

class UpdateExpenseTest {

    private val grid = GridFactory.create()
    private val expense = ExpenseFactory.create()
    private val updatedAmountOne = 12.5f
    private val updatedAmountTwo = 50f
    private val updatedUserIdOne = gridUserId(3)
    private val updatedUserIdTwo = gridUserId(1)
    private val updatedPayments = PaymentFactory.createList(updatedUserIdOne, updatedUserIdTwo, amountOne = updatedAmountOne, amountTwo = updatedAmountTwo)
    private val updatedDescription = "new description"
    private val updateExpensePolicy = createValidPolicy()
    private val payments = updatedPayments.map { UpdateExpense.UpdatedExpense.Payment(it.userId, it.amount) }
    private val expectedExpense = ExpenseFactory.create(paymentList = updatedPayments, description = updatedDescription)
    private val updatedExpense = UpdateExpense.UpdatedExpense(expense.id, expense.gridId, payments, updatedDescription)

    private val expenseRepository = InMemoryExpenseRepository()
    private val gridRepository = InMemoryGridRepository()
    private val transactionService = InMemoryTransactionService()

    private val updateExpense by lazy { UpdateExpense(expenseRepository, updateExpensePolicy, gridRepository, transactionService) }
    private lateinit var updateTestObserver: TestObserver<Void>

    @Test
    fun `modify existing expense`() {
        givenAGrid()
        givenAnExpense()

        whenUpdateIt()

        thenExpenseIsUpdated()
    }

    @Test
    fun `updating an expense should be update balance`() {
        givenAGrid()
        givenAnExpense()

        whenUpdateIt()

        thenBalanceIsUpdated()
    }

    @Test
    fun `modify invalid expense`() {
        givenAnExpense()
        givenARejectedPolicy()

        whenUpdateIt()

        thenUpdateFails()
    }

    private fun givenAGrid() {
        gridRepository.put(grid).blockingAwait()
    }

    private fun givenARejectedPolicy() {
        given(updateExpensePolicy.apply(updatedExpense)).willReturn(Completable.error(RuntimeException()))
    }

    private fun givenAnExpense() {
        expenseRepository.put(expense).blockingAwait()
    }


    private fun whenUpdateIt() {
        updateTestObserver = updateExpense(updatedExpense).test().await()
    }

    private fun thenExpenseIsUpdated() {
        val expenseFound = expenseRepository.find(expense.id).blockingGet()
        updateTestObserver.assertComplete()
        then(expenseFound).isEqualTo(expectedExpense)
    }

    private fun thenUpdateFails() {
        val expenseFound = expenseRepository.find(expense.id).blockingGet()
        updateTestObserver.assertNotComplete()
        then(expenseFound).isEqualTo(expense)
    }

    private fun thenBalanceIsUpdated() {
        val gridFound = gridRepository.find(grid.id).blockingGet()
        val updatedBalance = gridFound.findBalance(expense.userIds)
        val newBalance = gridFound.findBalance(expectedExpense.userIds)

        then(updatedBalance).isNull()
        then(newBalance).isNotNull
        then(newBalance!!.amount).isEqualTo(expectedExpense.amount)
    }

    private fun createValidPolicy() =
            Mockito.mock(UpdateExpensePolicy::class.java).also { given(it.apply(anyKotlin())).willReturn(Completable.complete()) }
}
