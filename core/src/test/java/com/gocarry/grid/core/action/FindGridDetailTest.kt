package com.gocarry.grid.core.action

import com.gocarry.grid.core.domain.repository.InMemoryGridUserRepository
import com.gocarry.grid.core.domain.service.GridExistenceValidator
import com.gocarry.grid.test.core.domain.factory.GridUserFactory
import com.gocarry.grid.test.extension.anyKotlin
import io.reactivex.Completable
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock

class FindGridDetailTest {

    private val gridUser = GridUserFactory.create()
    private val expectedGridUserResponse = FindGridDetail.GridUserResponse(gridUser.id, gridUser.userName)

    private val gridExistenceValidator = createGridExistenceValidator()

    private val gridUserRepository = InMemoryGridUserRepository()
    private val findGridUsers by lazy { FindGridDetail(gridExistenceValidator, gridUserRepository) }

    private lateinit var findTestObserver: TestObserver<FindGridDetail.GridDetailResponse>
    @Test
    fun `find should return all grid users`() {
        givenAGridUser()

        whenFind()

        thenAGridUsersAreFound()
    }

    @Test
    fun `find should fail when grid not exist`() {
        givenAGridUser()
        givenANoExistentGrid()

        whenFind()

        thenFindFails()
    }

    private fun givenANoExistentGrid() {
        given(gridExistenceValidator.validate(anyKotlin())).willReturn(Completable.error(RuntimeException()))
    }

    private fun givenAGridUser() {
        gridUserRepository.put(gridUser).blockingAwait()
    }

    private fun whenFind() {
        findTestObserver = findGridUsers(gridUser.gridId).test().await()
    }

    private fun thenAGridUsersAreFound() {
        val gridDetail = findTestObserver.values().first()
        findTestObserver.assertComplete()
        then(gridDetail.users).containsExactlyInAnyOrder(expectedGridUserResponse)
    }

    private fun thenFindFails() {
        findTestObserver.assertNotComplete()
    }

    private fun createGridExistenceValidator(): GridExistenceValidator {
        return mock(GridExistenceValidator::class.java)
                .also { given(it.validate(anyKotlin())).willReturn(Completable.complete()) }
    }
}