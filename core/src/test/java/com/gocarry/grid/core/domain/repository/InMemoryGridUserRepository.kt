package com.gocarry.grid.core.domain.repository

import com.gocarry.grid.core.domain.GridUser
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

class InMemoryGridUserRepository : GridUserRepository {

    private val gridUsers: MutableList<GridUser> = mutableListOf()

    override fun put(gridUser: GridUser): Completable {
        return Completable.fromAction { gridUsers.add(gridUser) }
    }

    override fun putAll(gridUserList: List<GridUser>): Completable {
        return Completable.fromAction { this.gridUsers.addAll(gridUserList) }
    }

    override fun find(id: String, gridId: String): Maybe<GridUser> {
        return Maybe.fromCallable { gridUsers.find { it.id == id && it.gridId == gridId } }
    }

    override fun findAllByGrid(gridId: String): Single<List<GridUser>> {
        return Single.fromCallable { gridUsers.filter { it.gridId == gridId } }
    }

    override fun removeByGrid(gridId: String): Completable {
        return Completable.fromAction { gridUsers.removeIf { it.gridId == gridId } }
    }

    override fun remove(id: String, gridId: String): Completable {
        return Completable.fromAction { gridUsers.removeIf { it.id == id && it.gridId == gridId } }
    }
}