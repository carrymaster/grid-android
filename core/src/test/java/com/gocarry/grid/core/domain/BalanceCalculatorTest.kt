package com.gocarry.grid.core.domain

import com.gocarry.grid.core.domain.service.BalanceCalculator
import com.gocarry.grid.test.core.domain.factory.BalanceFactory
import com.gocarry.grid.test.core.domain.factory.ExpenseFactory
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test

class BalanceCalculatorTest {

    private val expense = ExpenseFactory.create()
    private val expectedBalance = BalanceFactory.create()
    private lateinit var inverseBalance: Balance
    private lateinit var balance: Balance

    private val inverseBalanceCalculator by lazy { BalanceCalculator() }

    @Test
    fun `calculate balance`() {
        whenCalculateBalance()
        thenBalanceIsValid()
    }

    @Test
    fun `calculate inverse balance`() {
        whenCalculateInverse()
        thenInverseBalanceIsValid()
    }

    private fun whenCalculateInverse() {
        inverseBalance = inverseBalanceCalculator.calculateInverse(expense)
    }

    private fun whenCalculateBalance() {
        balance = inverseBalanceCalculator.calculate(expense)
    }

    private fun thenBalanceIsValid() {
        then(balance).isNotNull
        then(balance.amount).isEqualTo(expectedBalance.amount)
        then(balance.userIds).isEqualTo(expectedBalance.userIds)
        then(balance.userBalances.map { it.balanceAmount }).isEqualTo(expectedBalance.userBalances.map { it.balanceAmount })
    }

    private fun thenInverseBalanceIsValid() {
        then(inverseBalance).isNotNull
        then(inverseBalance.amount).isEqualTo(expectedBalance.amount * -1)
        then(inverseBalance.userIds).isEqualTo(expectedBalance.userIds)
        then(inverseBalance.userBalances.map { it.balanceAmount }).isEqualTo(expectedBalance.userBalances.map { it.balanceAmount * -1 })
    }

}