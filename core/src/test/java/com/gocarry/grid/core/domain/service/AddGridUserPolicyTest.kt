package com.gocarry.grid.core.domain.service

import com.gocarry.grid.core.action.AddGridUser
import com.gocarry.grid.test.extension.anyKotlin
import io.reactivex.Completable
import io.reactivex.observers.TestObserver
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito

class AddGridUserPolicyTest {

    private lateinit var userToAdd: AddGridUser.GridUserToAdd
    private lateinit var policyTestObserver: TestObserver<Void>
    private val gridExistenceValidator = createGridExistenceValidator()

    private val addGridUserPolicy = AddGridUserPolicy(gridExistenceValidator)

    @Test
    fun `validation should pass when user is valid`() {
        givenAValidUserToAdd()

        whenApplyPolicy()

        thenValidationPass()
    }

    @Test
    fun `validation should fail when user has an empty userId`() {
        givenAUserToAddWithEmptyId()

        whenApplyPolicy()

        thenValidationFails("user-id cannot be empty")
    }

    @Test
    fun `validation should fail when user has an empty userName`() {
        givenAUserToAddWithEmptyUserName()

        whenApplyPolicy()

        thenValidationFails("user-name cannot be empty")
    }

    @Test
    fun `validation should fail when grid does not exist`() {
        givenAValidUserToAdd()
        givenNotExistenGrid()

        whenApplyPolicy()

        thenValidationFails()
    }

    private fun givenNotExistenGrid() {
        given(gridExistenceValidator.validate(anyKotlin())).willReturn(Completable.error(RuntimeException()))
    }

    private fun givenAUserToAddWithEmptyUserName() {
        userToAdd = AddGridUser.GridUserToAdd("gridId", "userId", "")
    }

    private fun givenAUserToAddWithEmptyId() {
        userToAdd = AddGridUser.GridUserToAdd("gridId", "", "Romina Silvero")
    }

    private fun givenAValidUserToAdd() {
        userToAdd = AddGridUser.GridUserToAdd("gridId", "userId", "Romina Silvero")
    }

    private fun whenApplyPolicy() {
        policyTestObserver = addGridUserPolicy.apply(userToAdd).test().await()
    }

    private fun thenValidationPass() {
        policyTestObserver.assertComplete()
    }

    private fun thenValidationFails(message: String? = null) {
        policyTestObserver.assertNotComplete()
        message?.also { policyTestObserver.assertErrorMessage(it) }
    }

    private fun createGridExistenceValidator(): GridExistenceValidator {
        return Mockito.mock(GridExistenceValidator::class.java)
                .also { given(it.validate(anyKotlin())).willReturn(Completable.complete()) }
    }
}