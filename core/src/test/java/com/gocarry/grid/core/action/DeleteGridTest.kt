package com.gocarry.grid.core.action

import com.gocarry.grid.core.domain.repository.InMemoryDeleteUserRepository
import com.gocarry.grid.core.domain.repository.InMemoryExpenseRepository
import com.gocarry.grid.core.domain.repository.InMemoryGridRepository
import com.gocarry.grid.core.domain.repository.InMemoryGridUserRepository
import com.gocarry.grid.core.domain.service.GridExistenceValidator
import com.gocarry.grid.core.domain.service.InMemoryTransactionService
import com.gocarry.grid.test.core.domain.factory.*
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test

class DeleteGridTest {
    private val grid = GridFactory.create()
    private val gridUserOne = GridUserFactory.create(id = FactoryDefaultValues.gridUserId(1))
    private val gridUserTwo = GridUserFactory.create(id = FactoryDefaultValues.gridUserId(2))
    private val deletedUser = DeletedUserFactory.create(id = FactoryDefaultValues.gridUserId(3))
    private val expense = ExpenseFactory.create(paymentList = PaymentFactory.createList(FactoryDefaultValues.gridUserId(1), FactoryDefaultValues.gridUserId(2)))

    private val gridRepository = InMemoryGridRepository()
    private val gridUserRepository = InMemoryGridUserRepository()
    private val expenseRepository = InMemoryExpenseRepository()
    private val deletedUserRepository = InMemoryDeleteUserRepository()
    private val gridValidaService = GridExistenceValidator(gridRepository)
    private val transactionService = InMemoryTransactionService()

    private val deleteGrid by lazy { DeleteGrid(transactionService, gridValidaService, gridRepository, gridUserRepository, expenseRepository, deletedUserRepository) }
    private lateinit var deleteGridTestObserver: TestObserver<Void>

    @Test
    fun deleteGrid() {
        givenAGrid()
        givenTwoGridUsers()
        givenADeletedUser()
        givenAnExpense()

        whenDeleteGrid()

        thenAllAreDeleted()
    }

    private fun givenADeletedUser() {
        deletedUserRepository.put(deletedUser).blockingAwait()
    }

    private fun givenAGrid() {
        gridRepository.put(grid).blockingAwait()
    }

    private fun givenTwoGridUsers() {
        gridUserRepository.putAll(listOf(gridUserOne, gridUserTwo)).blockingAwait()
    }

    private fun givenAnExpense() {
        expenseRepository.put(expense).blockingAwait()
    }

    private fun whenDeleteGrid() {
        deleteGridTestObserver = deleteGrid(grid.id).test().await()
    }

    private fun thenAllAreDeleted() {
        val gridFound = gridRepository.find(grid.id).blockingGet()
        val userOneFound = gridUserRepository.find(gridUserOne.id, gridUserOne.gridId).blockingGet()
        val userTwoFound = gridUserRepository.find(gridUserTwo.id, gridUserTwo.gridId).blockingGet()
        val expenseFound = expenseRepository.find(expense.id).blockingGet()
        val deletedUserFound = deletedUserRepository.find(deletedUser.id, deletedUser.gridId).blockingGet()

        deleteGridTestObserver.assertComplete()
        then(gridFound).isNull()
        then(userOneFound).isNull()
        then(userTwoFound).isNull()
        then(expenseFound).isNull()
        then(deletedUserFound).isNull()
    }
}