package com.gocarry.grid.core.domain.policy

import com.gocarry.grid.core.domain.repository.InMemoryExpenseRepository
import com.gocarry.grid.core.domain.repository.InMemoryGridRepository
import com.gocarry.grid.core.domain.service.GridExistenceValidator
import com.gocarry.grid.test.core.domain.factory.ExpenseFactory
import com.gocarry.grid.test.core.domain.factory.GridFactory
import io.reactivex.observers.TestObserver
import org.junit.Test

class RemoveExpensePolicyTest {

    private val grid = GridFactory.create()
    private val expense = ExpenseFactory.create()

    private val gridRepository = InMemoryGridRepository()
    private val expenseRepository = InMemoryExpenseRepository()
    private val gridValidationService = GridExistenceValidator(gridRepository)

    private val removeExpensePolicy by lazy { RemoveExpensePolicy(gridValidationService, expenseRepository) }
    private lateinit var policyTestObserver: TestObserver<Void>

    @Test
    fun `apply policy to valid expense`() {
        givenAGrid()
        givenAnExpense()

        whenApply()

        thenPolicyIsCorrect()
    }

    @Test
    fun `apply policy when grid does not exist`() {
        givenAnExpense()

        whenApply()

        thenPolicyFails()
    }

    @Test
    fun `apply policy when expense does not exist`() {
        givenAGrid()

        whenApply()

        thenPolicyFails()
    }

    private fun givenAGrid() {
        gridRepository.put(grid).blockingAwait()
    }

    private fun givenAnExpense() {
        expenseRepository.put(expense).blockingAwait()
    }

    private fun whenApply() {
        policyTestObserver = removeExpensePolicy.apply(expense.id).test().await()
    }

    private fun thenPolicyIsCorrect() {
        policyTestObserver.assertComplete()

    }

    private fun thenPolicyFails() {
        policyTestObserver.assertNotComplete()
    }
}