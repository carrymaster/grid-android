package com.gocarry.grid.core.domain.repository

import com.gocarry.grid.core.domain.DeletedUser
import io.reactivex.Completable
import io.reactivex.Maybe

class InMemoryDeleteUserRepository : DeletedUserRepository {
    private val deleteUsers = mutableSetOf<DeletedUser>()

    override fun put(deleteUser: DeletedUser): Completable {
        return Completable.fromAction { deleteUsers.add(deleteUser) }
    }

    override fun find(userId: String, gridId: String): Maybe<DeletedUser> {
        return Maybe.fromCallable { deleteUsers.find { it.id == userId && it.gridId == gridId } }
    }

    override fun removeByGrid(gridId: String): Completable {
        return Completable.fromAction { deleteUsers.removeAll { it.gridId == gridId } }
    }
}