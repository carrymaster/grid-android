package com.gocarry.grid.core.action

import com.gocarry.grid.core.domain.Expense
import com.gocarry.grid.core.domain.policy.AddExpensePolicy
import com.gocarry.grid.core.domain.repository.InMemoryExpenseRepository
import com.gocarry.grid.core.domain.repository.InMemoryGridRepository
import com.gocarry.grid.core.domain.repository.InMemoryGridUserRepository
import com.gocarry.grid.core.domain.service.GridExistenceValidator
import com.gocarry.grid.core.domain.service.GridUserExistenceValidator
import com.gocarry.grid.core.domain.service.InMemoryIdGenerator
import com.gocarry.grid.core.domain.service.InMemoryTransactionService
import com.gocarry.grid.test.core.domain.factory.GridFactory
import com.gocarry.grid.test.core.domain.factory.GridUserFactory
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test

class AddExpenseTest {

    private val gridId = "grid id"

    private val expenseId = "expense id"
    private val expensePayments = listOf(AddExpense.ExpenseToAdd.Payment("userId1", 45.50f), AddExpense.ExpenseToAdd.Payment("userId2", 20.50f))
    private val expenseDescription = "Best Expense"

    private val gridRepository = InMemoryGridRepository()
    private val gridUserRepository = InMemoryGridUserRepository()
    private val expenseRepository = InMemoryExpenseRepository()
    private val expenseIdService = InMemoryIdGenerator(expenseId)
    private val gridValidationService = GridExistenceValidator(gridRepository)
    private val gridUserValidationService = GridUserExistenceValidator(gridUserRepository)
    private val expensePolicy = AddExpensePolicy(gridValidationService, gridUserValidationService)
    private val transactionService = InMemoryTransactionService()

    private val addExpense by lazy { AddExpense(transactionService, expenseIdService, expensePolicy, gridRepository, expenseRepository) }
    private lateinit var addExpenseObserver: TestObserver<Void>

    @Test
    fun validExpense() {
        givenAGrid()
        givenAGridUsers()
        whenAddExpense()
        thenExpenseWasAdded()
    }

    private fun givenAGridUsers() {
        expensePayments.map { GridUserFactory.create(it.userId, gridId) }
                .forEach { gridUserRepository.put(it).blockingGet() }
    }

    private fun givenAGrid() {
        val grid = GridFactory.create(id = gridId)
        gridRepository.put(grid).blockingGet()
    }

    private fun whenAddExpense() {
        val expenseToAddExpense = AddExpense.ExpenseToAdd(gridId, expensePayments, expenseDescription)
        addExpenseObserver = addExpense(expenseToAddExpense).test()
    }

    private fun thenExpenseWasAdded() {
        thenExpenseIsSaved()
        thenBalanceExist()
    }

    private fun thenExpenseIsSaved(): Expense? {
        addExpenseObserver.assertNoErrors()
        val expense = expenseRepository.find(expenseId).blockingGet()
        then(expense).isNotNull
        then(expense.gridId).isEqualTo(gridId)
        then(expense.payments.map { it.userId }).isEqualTo(expensePayments.map { it.userId })
        then(expense.payments.map { it.amount }).isEqualTo(expensePayments.map { it.amount })
        then(expense.description).isEqualTo(expenseDescription)
        return expense
    }

    private fun thenBalanceExist() {
        val grid = gridRepository.find(gridId).blockingGet()
        val expense = expenseRepository.find(expenseId).blockingGet()
        val balance = grid.findBalance(expense.userIds)
        then(balance).isNotNull
    }
}

