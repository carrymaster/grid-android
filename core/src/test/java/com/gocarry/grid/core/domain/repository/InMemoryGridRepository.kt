package com.gocarry.grid.core.domain.repository

import com.gocarry.grid.core.domain.Grid
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

class InMemoryGridRepository : GridRepository {
    private val grids: MutableSet<Grid> = mutableSetOf()

    override fun put(grid: Grid): Completable {
        return Completable.fromAction {
            if (grids.contains(grid)) grids.remove(grid)
            grids.add(grid)
        }
    }

    override fun find(id: String): Maybe<Grid> {
        return Maybe.fromCallable { grids.find { it.id == id } }
    }

    override fun remove(id: String): Completable {
        return Completable.fromAction { grids.removeIf { it.id == id } }
    }

    override fun findAll(): Single<List<Grid>> {
        return Single.fromCallable { grids.toList() }
    }

}