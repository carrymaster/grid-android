package com.gocarry.grid.core.domain.service

import com.gocarry.grid.core.domain.repository.InMemoryGridRepository
import com.gocarry.grid.test.core.domain.factory.GridFactory
import io.reactivex.observers.TestObserver
import org.junit.Test

class GridExistenceValidatorTest {

    private val gridId = "gridId"
    private val grid = GridFactory.create(id = gridId)
    private val gridRepository = InMemoryGridRepository()
    private val gridValidationService by lazy { GridExistenceValidator(gridRepository) }
    private lateinit var validationObserver: TestObserver<Void>

    @Test
    fun gridExist() {
        givenAGrid()
        whenValidate()
        thenValidationSuccess()
    }

    @Test
    fun gridNotExist() {
        whenValidate()
        thenGridNotExistExceptionWasThrown()
    }

    private fun givenAGrid() {
        gridRepository.put(grid).blockingGet()
    }

    private fun whenValidate() {
        validationObserver = gridValidationService.validate(gridId).test()
    }

    private fun thenValidationSuccess() {
        validationObserver.assertNoErrors()
        validationObserver.assertComplete()
    }

    private fun thenGridNotExistExceptionWasThrown() {
        validationObserver.assertError(GridNotExistException::class.java)
    }

}