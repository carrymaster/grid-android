package com.gocarry.grid.core.domain.repository

import com.gocarry.grid.core.domain.Expense
import io.reactivex.Completable
import io.reactivex.Maybe

class InMemoryExpenseRepository : ExpenseRepository {
    private val expenses = mutableSetOf<Expense>()

    override fun put(expense: Expense): Completable {
        return Completable.fromAction { expenses.add(expense) }
    }

    override fun find(id: String): Maybe<Expense> {
        return Maybe.fromCallable { expenses.find { it.id == id } }
    }

    override fun remove(id: String): Completable {
        return Completable.fromAction { expenses.removeIf { it.id == id } }
    }

    override fun removeByGrid(gridId: String): Completable {
        return Completable.fromAction { expenses.removeIf { it.gridId == gridId } }
    }

    override fun update(expense: Expense): Completable {
        return Completable.fromAction {
            expenses.removeAll { it.id == expense.id }
            expenses.add(expense)
        }
    }
}