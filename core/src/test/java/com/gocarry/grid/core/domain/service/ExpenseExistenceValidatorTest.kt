package com.gocarry.grid.core.domain.service

import com.gocarry.grid.core.domain.repository.InMemoryExpenseRepository
import com.gocarry.grid.test.core.domain.factory.ExpenseFactory
import io.reactivex.observers.TestObserver
import org.junit.Test

class ExpenseExistenceValidatorTest {

    private val expense = ExpenseFactory.create()

    private val expenseRepository = InMemoryExpenseRepository()

    private val expenseExistenceValidator by lazy { ExpenseExistenceValidator(expenseRepository) }
    private lateinit var validationTestObserver: TestObserver<Void>

    @Test
    fun `validate existing expense`() {
        givenAnExpense()

        whenValidateIt()

        thenItIsValid()
    }

    @Test
    fun `validate not existing expense`() {
        whenValidateIt()

        thenItIsInvalid()
    }

    private fun givenAnExpense() {
        expenseRepository.put(expense).blockingAwait()
    }

    private fun whenValidateIt() {
        validationTestObserver = expenseExistenceValidator.validate(expense.id).test().await()
    }

    private fun thenItIsValid() {
        validationTestObserver.assertComplete()
    }

    private fun thenItIsInvalid() {
        validationTestObserver.assertNotComplete()
        validationTestObserver.assertError(ExpenseNotExistException::class.java)
    }
}