package com.gocarry.grid.core.domain

import com.gocarry.grid.test.core.domain.factory.ExpenseFactory
import com.gocarry.grid.test.core.domain.factory.GridFactory
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test

class GridTest {

    private val initialUserCount = 4
    private val grid = GridFactory.create(balanceSet = mutableSetOf(), userCount = initialUserCount)
    private val expense = ExpenseFactory.create()
    private val userIdSet = expense.userIds
    private lateinit var findBalance: () -> Balance?
    private lateinit var getTotalSpent: () -> Float

    @Test
    fun `balance not exist`() {
        whenFindBalance()
        thenBalanceNotExist()
    }

    @Test
    fun `balance exists`() {
        givenAnAddedExpense()
        whenFindBalance()
        thenBalanceIsReturn()
    }

    @Test
    fun `balance not exist after remove last expense`() {
        givenAnAddedExpense()
        givenAnRemovedExpense()
        whenFindBalance()
        thenBalanceNotExist()
    }

    @Test
    fun `increment user count`() {
        whenIncrementUserCount()
        thenUserCountWasIncremented()
    }

    @Test
    fun `decrement user count`() {
        whenDecrementUserCount()
        thenUserCountWasDecremented()
    }

    @Test
    fun `get total spent`() {
        givenAnAddedExpense()
        whenGetTotalSpent()
        thenTotalSpentIsCorrect()
    }

    private fun givenAnRemovedExpense() {
        grid.remove(expense)
    }

    private fun givenAnAddedExpense() {
        grid.add(expense)
    }

    private fun whenFindBalance() {
        findBalance = { grid.findBalance(userIdSet) }
    }

    private fun whenGetTotalSpent() {
        getTotalSpent = { grid.totalSpent }
    }

    private fun whenIncrementUserCount() {
        grid.incrementUserCount()
    }

    private fun whenDecrementUserCount() {
        grid.decrementUserCount()
    }

    private fun thenUserCountWasIncremented() {
        then(grid.userCount).isEqualTo(initialUserCount + 1)
    }

    private fun thenBalanceNotExist() {
        then(findBalance()).isNull()
    }

    private fun thenUserCountWasDecremented() {
        then(grid.userCount).isEqualTo(initialUserCount - 1)
    }

    private fun thenBalanceIsReturn() {
        then(findBalance()).isNotNull
        then(findBalance()!!.userIds).isEqualTo(userIdSet)
        then(findBalance()!!.amount).isEqualTo(expense.amount)
    }

    private fun thenTotalSpentIsCorrect() {
        then(getTotalSpent()).isEqualTo(expense.amount)
    }
}

