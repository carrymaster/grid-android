package com.gocarry.grid.core.action

import com.gocarry.grid.core.action.DeleteGridUser.UserToDelete
import com.gocarry.grid.core.domain.repository.InMemoryDeleteUserRepository
import com.gocarry.grid.core.domain.repository.InMemoryGridRepository
import com.gocarry.grid.core.domain.repository.InMemoryGridUserRepository
import com.gocarry.grid.core.domain.service.GridUserExistenceValidator
import com.gocarry.grid.core.domain.service.InMemoryTransactionService
import com.gocarry.grid.test.core.domain.factory.GridFactory
import com.gocarry.grid.test.core.domain.factory.GridUserFactory
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test

class DeleteGridUserTest {

    private val initialUserCount = 1
    private val grid = GridFactory.create(userCount = initialUserCount)
    private val gridUser = GridUserFactory.create()
    private val otherGridUser = GridUserFactory.create(gridId = "otherGridId")
    private val userToDelete = UserToDelete(gridUser.id, gridUser.gridId)

    private val gridUserRepository = InMemoryGridUserRepository()
    private val gridRepository = InMemoryGridRepository()
    private val deleteUserRepository = InMemoryDeleteUserRepository()
    private val transactionService = InMemoryTransactionService()
    private val gridUserValidationService = GridUserExistenceValidator(gridUserRepository)

    private val deleteGridUser by lazy { DeleteGridUser(gridUserRepository, deleteUserRepository, transactionService, gridUserValidationService, gridRepository) }
    private lateinit var deleteTestObserver: TestObserver<Void>

    @Test
    fun `delete user from a grid`() {
        givenAGrid()
        givenAGridUser()

        whenDeleteUser()

        thenUserIsDeleted()
    }

    @Test
    fun `delete user from another grid`() {
        givenAGridUserFromOtherGrid()

        whenDeleteUser()

        thenDeleteFails()
    }

    @Test
    fun `delete user should decrement user count from grid`() {
        givenAGrid()
        givenAGridUser()

        whenDeleteUser()

        thenUserCountIsUpdated()
    }

    private fun givenAGrid() {
        gridRepository.put(grid).blockingAwait()
    }

    private fun givenAGridUserFromOtherGrid() {
        gridUserRepository.put(otherGridUser).blockingAwait()
    }

    private fun givenAGridUser() {
        gridUserRepository.put(gridUser).blockingAwait()
    }

    private fun whenDeleteUser() {
        deleteTestObserver = deleteGridUser.invoke(userToDelete).test().await()
    }

    private fun thenUserIsDeleted() {
        val gridUserFound = gridUserRepository.find(gridUser.id, gridUser.id).blockingGet()
        val deleteUserFound = deleteUserRepository.find(gridUser.id, gridUser.gridId).blockingGet()
        deleteTestObserver.assertComplete()
        then(gridUserFound).isNull()
        then(deleteUserFound).isNotNull
    }

    private fun thenDeleteFails() {
        deleteTestObserver.assertNotComplete()
    }

    private fun thenUserCountIsUpdated() {
        val gridFound = gridRepository.find(grid.id).blockingGet()
        then(gridFound.userCount).isEqualTo(initialUserCount - 1)
    }
}
