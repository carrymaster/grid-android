package com.gocarry.grid.core.domain.service

class InMemoryIdGenerator(private val id: String) : IdGenerator {
    override fun generate(): String = id
}