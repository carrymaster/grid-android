package com.gocarry.grid.core.action

import com.gocarry.grid.core.domain.repository.InMemoryGridRepository
import com.gocarry.grid.core.domain.repository.InMemoryGridUserRepository
import com.gocarry.grid.test.core.domain.factory.FactoryDefaultValues.gridUserId
import com.gocarry.grid.test.core.domain.factory.GridFactory
import com.gocarry.grid.test.core.domain.factory.GridUserFactory
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.assertThat
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test

class FindGridsTest {

    private val grids = listOf(GridFactory.create(id = "gridOne"), GridFactory.create(id = "gridTwo"))
    private val gridUserIds = listOf(gridUserId(1), gridUserId(2))

    private val gridRepository = InMemoryGridRepository()
    private val gridUserRepository = InMemoryGridUserRepository()

    private val findGrids by lazy { FindGrids(gridRepository) }
    private lateinit var findTestObserver: TestObserver<List<FindGrids.GridResponse>>

    @Test
    fun `find grid when two are created`() {
        givenTwoGrids()
        givenGridUsers()

        whenFind()

        thenTwoGridsAreReturned()
    }

    private fun givenTwoGrids() {
        grids.forEach { gridRepository.put(it).blockingAwait() }
    }

    private fun givenGridUsers() {
        grids.forEach { grid ->
            gridUserIds.forEach { userId ->
                gridUserRepository.put(GridUserFactory.create(id = userId, gridId = grid.id)).blockingAwait()
            }
        }
    }

    private fun whenFind() {
        findTestObserver = findGrids().test().await()
    }

    private fun thenTwoGridsAreReturned() {
        findTestObserver.assertComplete()
        assertFindResult()
    }

    private fun assertFindResult() {
        val gridResponses = findTestObserver.values().first()
        then(gridResponses).hasSameSizeAs(grids)
        grids.forEach { grid ->
            then(gridResponses).anySatisfy { assertThat(it.id).isEqualTo(grid.id) }
            val gridResponse = gridResponses.find { it.id == grid.id }!!
            then(gridResponse.userCount).isEqualTo(gridUserIds.size)
            then(gridResponse.totalSpent).isEqualTo(grid.totalSpent)
        }
    }
}