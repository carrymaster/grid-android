package com.gocarry.grid.core.domain

import com.gocarry.grid.core.domain.service.BalanceCalculator
import com.gocarry.grid.test.core.domain.factory.BalanceFactory
import com.gocarry.grid.test.core.domain.factory.ExpenseFactory
import com.gocarry.grid.test.core.domain.factory.PaymentFactory
import org.assertj.core.api.BDDAssertions.catchThrowable
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test

class BalanceTest {

    private val userId = "userId"
    private val anotherUserId = "anotherUserId"
    private val userPaymentAmount = 15.20f
    private val anotherUserPaymentAmount = 4.80f
    private val userSecondPaymentAmount = 35f
    private val anotherUserSecondPaymentAmount = 95.2f
    private val paymentList = PaymentFactory.createList(userId, anotherUserId, userPaymentAmount, anotherUserPaymentAmount)
    private val secondPaymentList = PaymentFactory.createList(userId, anotherUserId, userSecondPaymentAmount, anotherUserSecondPaymentAmount)
    private val otherBalanceExpensePaymentList = PaymentFactory.createList("ohterUserId", userId, 30f, 20f)
    private val expense = ExpenseFactory.create(paymentList = paymentList)
    private val secondExpense = ExpenseFactory.create(paymentList = secondPaymentList)
    private val otherBalanceExpense = ExpenseFactory.create(paymentList = otherBalanceExpensePaymentList)
    private val balance = BalanceFactory.createInitial(setOf(userId, anotherUserId))
    private var expectedException: Throwable? = null

    @Test
    fun `update balance first time`() {
        whenUpdateBalance(expense)
        thenInitialBalanceIsCorrect()
    }

    @Test
    fun `update a balance twice`() {
        givenABalanceUpdated()
        whenUpdateBalance(secondExpense)
        thenBalanceIsCorrect()
    }

    @Test
    fun `update balance with other balance's expense`() {
        givenABalanceUpdated()
        whenUpdateBalance(otherBalanceExpense)
        thenInvalidBalanceExceptionIsThrow()
    }

    private fun givenABalanceUpdated() {
        balance.update(getBalanceOf(expense))
    }

    private fun whenUpdateBalance(expense: Expense) {
        expectedException = catchThrowable { balance.update(getBalanceOf(expense)) }
    }

    private fun thenInitialBalanceIsCorrect() {
        then(balance.amount).isEqualTo(expense.amount)
        then(balance.userBalances).isEqualTo(getBalanceOf(expense).userBalances)
    }

    private fun thenBalanceIsCorrect() {
        val expectedBalance = getBalanceOf(expense).also { it.update(getBalanceOf(secondExpense)) }
        then(balance.amount).isEqualTo(expense.amount + secondExpense.amount)
        then(balance.userBalances).isEqualTo(expectedBalance.userBalances)
    }

    private fun thenInvalidBalanceExceptionIsThrow() {
        then(expectedException).isExactlyInstanceOf(InvalidBalanceUpdate::class.java)
    }

    private fun getBalanceOf(expense: Expense) = BalanceCalculator().calculate(expense)
}