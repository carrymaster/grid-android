package com.gocarry.grid.core.domain.policy

import com.gocarry.grid.core.action.UpdateExpense.UpdatedExpense
import com.gocarry.grid.core.action.UpdateExpense.UpdatedExpense.Payment
import com.gocarry.grid.core.domain.repository.InMemoryExpenseRepository
import com.gocarry.grid.core.domain.repository.InMemoryGridUserRepository
import com.gocarry.grid.core.domain.service.ExpenseExistenceValidator
import com.gocarry.grid.core.domain.service.GridUserExistenceValidator
import com.gocarry.grid.test.core.domain.factory.ExpenseFactory
import com.gocarry.grid.test.core.domain.factory.GridUserFactory
import io.reactivex.observers.TestObserver
import org.junit.Test

class UpdateExpensePolicyTest {

    private val expense = ExpenseFactory.create()
    private val gridUserOne = GridUserFactory.create(id = expense.userIds.first())
    private val gridUserTwo = GridUserFactory.create(id = expense.userIds.last())
    private val updatedExpense = UpdatedExpense(expense.id, expense.gridId, expense.payments.map { Payment(it.userId, it.amount) }, expense.description)

    private val expenseRepository = InMemoryExpenseRepository()
    private val gridUserRepository = InMemoryGridUserRepository()
    private val gridUserExistenceValidator = GridUserExistenceValidator(gridUserRepository)
    private val expenseExistenceValidator = ExpenseExistenceValidator(expenseRepository)

    private val updateExpensePolicy by lazy { UpdateExpensePolicy(expenseExistenceValidator, gridUserExistenceValidator) }
    private lateinit var updateTestObserver: TestObserver<Void>

    @Test
    fun `apply on valid expense`() {
        givenAnExpense()
        givenGridUsers()

        whenApplyPolicy()

        thenPolicyIsValid()
    }

    @Test
    fun `apply on not existent expense`() {
        givenGridUsers()

        whenApplyPolicy()

        thenPolicyIsInvalid()
    }

    @Test
    fun `apply on not existent users`() {
        givenAnExpense()

        whenApplyPolicy()

        thenPolicyIsInvalid()
    }

    private fun givenAnExpense() {
        expenseRepository.put(expense).blockingAwait()
    }

    private fun givenGridUsers() {
        gridUserRepository.putAll(listOf(gridUserOne, gridUserTwo)).blockingAwait()
    }

    private fun whenApplyPolicy() {
        updateTestObserver = updateExpensePolicy.apply(updatedExpense).test().await()
    }

    private fun thenPolicyIsValid() {
        updateTestObserver.assertComplete()
    }

    private fun thenPolicyIsInvalid() {
        updateTestObserver.assertNotComplete()
    }
}

