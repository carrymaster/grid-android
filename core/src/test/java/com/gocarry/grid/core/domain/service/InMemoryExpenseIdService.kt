package com.gocarry.grid.core.domain.service

import io.reactivex.Single

class InMemoryExpenseIdService(private val expenseId: String) : ExpenseIdService {
    override fun create(): Single<String> {
        return Single.just(expenseId)
    }

}