package com.gocarry.grid.core.domain.service

import io.reactivex.Completable

class InMemoryTransactionService : TransactionService {
    override fun runOnTransaction(operation: () -> Unit): Completable {
        return Completable.fromAction { operation.invoke() }
    }
}