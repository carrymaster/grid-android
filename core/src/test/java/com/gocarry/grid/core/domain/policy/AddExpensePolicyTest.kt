package com.gocarry.grid.core.domain.policy

import com.gocarry.grid.core.action.AddExpense
import com.gocarry.grid.core.action.AddExpense.ExpenseToAdd.Payment
import com.gocarry.grid.core.domain.repository.InMemoryGridRepository
import com.gocarry.grid.core.domain.repository.InMemoryGridUserRepository
import com.gocarry.grid.core.domain.service.GridExistenceValidator
import com.gocarry.grid.core.domain.service.GridNotExistException
import com.gocarry.grid.core.domain.service.GridUserExistenceValidator
import com.gocarry.grid.core.domain.service.GridUserNotExistException
import com.gocarry.grid.test.core.domain.factory.GridFactory
import com.gocarry.grid.test.core.domain.factory.GridUserFactory
import io.reactivex.observers.TestObserver
import org.junit.Test

class AddExpensePolicyTest {

    private val grid = GridFactory.create()
    private val paymentList = listOf(Payment("userId1", 20.5f), Payment("userId2", 22.5f))
    private val description = "description"

    private val gridRepository = InMemoryGridRepository()
    private val gridUserRepository = InMemoryGridUserRepository()
    private val gridValidationService = GridExistenceValidator(gridRepository)
    private val gridUserValidationService = GridUserExistenceValidator(gridUserRepository)

    private val addExpensePolicy by lazy { AddExpensePolicy(gridValidationService, gridUserValidationService) }
    private lateinit var expenseToAdd: AddExpense.ExpenseToAdd
    private lateinit var policyObserver: TestObserver<Void>

    @Test
    fun validExpense() {
        givenAGrid()
        givenAGridUsers()
        givenAExpenseToAdd()
        whenApplyPolicy()
        thenExpenseToAddWasValid()
    }

    @Test
    fun noGridExist() {
        givenAGridUsers()
        givenAExpenseToAdd()
        whenApplyPolicy()
        thenNoGridExistExceptionWasThrown()
    }

    @Test
    fun userDoesNotBelongToGrid() {
        givenAGrid()
        givenAExpenseToAdd()
        whenApplyPolicy()
        thenGridUserNotExistException()
    }

    private fun givenAGridUsers() {
        paymentList.forEach { gridUserRepository.put(GridUserFactory.create(id = it.userId, gridId = grid.id)).blockingGet() }
    }

    private fun givenAGrid() {
        gridRepository.put(grid).blockingGet()
    }

    private fun givenAExpenseToAdd() {
        expenseToAdd = AddExpense.ExpenseToAdd(grid.id, paymentList, description)
    }

    private fun whenApplyPolicy() {
        policyObserver = addExpensePolicy.apply(expenseToAdd).test()
    }

    private fun thenExpenseToAddWasValid() {
        policyObserver.assertComplete()
        policyObserver.assertNoErrors()
    }

    private fun thenNoGridExistExceptionWasThrown() {
        policyObserver.assertError(GridNotExistException::class.java)
    }

    private fun thenGridUserNotExistException() {
        policyObserver.assertError(GridUserNotExistException::class.java)
    }
}


