package com.gocarry.grid.core.action

import com.gocarry.grid.core.domain.policy.RemoveExpensePolicy
import com.gocarry.grid.core.domain.repository.InMemoryExpenseRepository
import com.gocarry.grid.core.domain.repository.InMemoryGridRepository
import com.gocarry.grid.core.domain.repository.InMemoryGridUserRepository
import com.gocarry.grid.core.domain.service.GridExistenceValidator
import com.gocarry.grid.core.domain.service.InMemoryTransactionService
import com.gocarry.grid.test.core.domain.factory.ExpenseFactory
import com.gocarry.grid.test.core.domain.factory.FactoryDefaultValues.gridUserId
import com.gocarry.grid.test.core.domain.factory.GridFactory
import com.gocarry.grid.test.core.domain.factory.GridUserFactory
import com.gocarry.grid.test.core.domain.factory.PaymentFactory
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test

class DeleteExpenseTest {

    private val grid = GridFactory.create(balanceSet = mutableSetOf())
    private val differentGrid = GridFactory.create(id = "differentGridId")
    private val gridUserOne = GridUserFactory.create(id = gridUserId(1))
    private val gridUserTwo = GridUserFactory.create(id = gridUserId(2))
    private val expense = ExpenseFactory.create(paymentList = PaymentFactory.createList(gridUserOne.id, gridUserTwo.id))

    private val gridRepository = InMemoryGridRepository()
    private val expenseRepository = InMemoryExpenseRepository()
    private val gridUserRepository = InMemoryGridUserRepository()
    private val gridValidationService = GridExistenceValidator(gridRepository)
    private val removePolicy = RemoveExpensePolicy(gridValidationService, expenseRepository)
    private val transactionService = InMemoryTransactionService()

    private val deleteExpense by lazy { DeleteExpense(gridRepository, expenseRepository, removePolicy, transactionService) }
    private lateinit var deleteExpenseTestObserver: TestObserver<Void>

    @Test
    fun `delete expense should update balance`() {
        givenAGridWithBalance()
        givenAGridUsers()
        givenAnExpense()

        whenDelete()

        thenBalanceIsCorrect()
    }

    @Test
    fun `delete expense should remove it`() {
        givenAGridWithBalance()
        givenAGridUsers()
        givenAnExpense()

        whenDelete()

        thenExpenseIsRemoved()
    }

    @Test
    fun `delete invalid expense should fail`() {
        givenADifferentGrid()
        givenAGridUsers()
        givenAnExpense()

        whenDelete()

        thenDeleteFails()
    }

    private fun givenAGridUsers() {
        gridUserRepository.putAll(listOf(gridUserOne, gridUserTwo)).blockingAwait()
    }

    private fun givenADifferentGrid() {
        gridRepository.put(differentGrid).blockingAwait()
    }

    private fun givenAnExpense() {
        expenseRepository.put(expense).blockingAwait()
    }

    private fun givenAGridWithBalance() {
        grid.add(expense)
        gridRepository.put(grid).blockingAwait()
    }

    private fun whenDelete() {
        deleteExpenseTestObserver = deleteExpense(expense.id).test().await()
    }

    private fun thenBalanceIsCorrect() {
        val gridFound = gridRepository.find(grid.id).blockingGet()
        deleteExpenseTestObserver.assertComplete()
        then(gridFound.balanceSet).isEmpty()
    }

    private fun thenExpenseIsRemoved() {
        val expenseFound = expenseRepository.find(expense.id).blockingGet()
        deleteExpenseTestObserver.assertComplete()
        then(expenseFound).isNull()
    }

    private fun thenDeleteFails() {
        deleteExpenseTestObserver.assertNotComplete()
    }
}