package com.gocarry.grid.core.domain.service

import com.gocarry.grid.core.action.CreateGrid
import io.reactivex.observers.TestObserver
import org.junit.Test

class GridToCreateValidatorTest {

    private lateinit var gridToCreate: CreateGrid.GridToCreate
    private val gridToCreateValidator by lazy { GridToCreateValidator() }
    private lateinit var validationTestObserver: TestObserver<Void>

    @Test
    fun `validate correct grid should success`() {
        givenACorrectGridToCreate()

        whenValidate()

        thenValidateSuccess()
    }

    @Test
    fun `empty description grid should fails`() {
        givenAnEmptyDescriptionGridToCreate()

        whenValidate()

        thenValidateFails("CreateGrid.GridToCreate.description cannot be empty")
    }

    @Test
    fun `empty creator id grid should fails`() {
        givenAnEmptyCreatorIdGridToCreate()

        whenValidate()

        thenValidateFails("CreateGrid.GridToCreate.Creator.id cannot be empty")
    }

    private fun givenAnEmptyCreatorIdGridToCreate() {
        gridToCreate = CreateGrid.GridToCreate("best grid", CreateGrid.GridToCreate.Creator("", "user name"))
    }

    private fun givenAnEmptyDescriptionGridToCreate() {
        gridToCreate = CreateGrid.GridToCreate("", CreateGrid.GridToCreate.Creator("best id", "user name"))
    }

    private fun givenACorrectGridToCreate() {
        gridToCreate = CreateGrid.GridToCreate("best grid", CreateGrid.GridToCreate.Creator("best id", "user name"))
    }

    private fun whenValidate() {
        validationTestObserver = gridToCreateValidator.validate(gridToCreate).test().await()
    }

    private fun thenValidateSuccess() {
        validationTestObserver.assertComplete()
    }

    private fun thenValidateFails(expectedErrorMessage: String) {
        validationTestObserver.assertNotComplete()
        validationTestObserver.assertErrorMessage(expectedErrorMessage)
    }
}