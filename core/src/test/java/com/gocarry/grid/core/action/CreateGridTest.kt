package com.gocarry.grid.core.action

import com.gocarry.grid.core.action.CreateGrid.GridToCreate
import com.gocarry.grid.core.action.CreateGrid.GridToCreate.Creator
import com.gocarry.grid.core.domain.repository.InMemoryGridRepository
import com.gocarry.grid.core.domain.repository.InMemoryGridUserRepository
import com.gocarry.grid.core.domain.service.GridToCreateValidator
import com.gocarry.grid.core.domain.service.InMemoryIdGenerator
import com.gocarry.grid.core.domain.service.InMemoryTransactionService
import com.gocarry.grid.test.core.domain.factory.GridFactory
import com.gocarry.grid.test.core.domain.factory.GridUserFactory
import com.gocarry.grid.test.extension.anyKotlin
import io.reactivex.Completable
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito

class CreateGridTest {

    private val grid = GridFactory.create(balanceSet = mutableSetOf(), userCount = 1)
    private val gridUser = GridUserFactory.create()

    private val gridIdService = InMemoryIdGenerator(grid.id)
    private val gridRepository = InMemoryGridRepository()
    private val gridUserRepository = InMemoryGridUserRepository()
    private val gridToCreateValidator = createGridToCreateValidator()
    private val transactionService = InMemoryTransactionService()

    private val createGrid by lazy { CreateGrid(gridRepository, gridIdService, gridUserRepository, gridToCreateValidator, transactionService) }
    private lateinit var creationTestObserver: TestObserver<Void>

    @Test
    fun `valid grid to create should be created`() {
        whenCreateAGrid()
        thenGridWasCreated()
    }

    @Test
    fun `invalid grid to create should fails`() {
        givenFailedGridToCreateValidator()
        whenCreateAGrid()
        thenCreationFails()
    }

    private fun givenFailedGridToCreateValidator() {
        given(gridToCreateValidator.validate(anyKotlin())).willReturn(Completable.error(RuntimeException()))
    }

    private fun whenCreateAGrid() {
        val gridToCreate = GridToCreate(grid.description, Creator(gridUser.id, gridUser.userName))
        creationTestObserver = createGrid(gridToCreate).test().await()
    }

    private fun thenGridWasCreated() {
        thenGridExists()
        thenGridUsersExits()
    }

    private fun thenGridUsersExits() {
        val gridUserFound = gridUserRepository.find(gridUser.id, gridUser.gridId).blockingGet()
        then(gridUserFound).isEqualTo(gridUserFound)
    }

    private fun thenGridExists() {
        val gridFound = gridRepository.find(grid.id).blockingGet()
        then(gridFound).isNotNull
        then(gridFound.description).isEqualTo(grid.description)
        then(gridFound.userCount).isEqualTo(grid.userCount)
    }

    private fun thenCreationFails() {
        val gridFound = gridRepository.find(grid.id).blockingGet()
        creationTestObserver.assertNotComplete()
        then(gridFound).isNull()
    }

    private fun createGridToCreateValidator(): GridToCreateValidator {
        return Mockito.mock(GridToCreateValidator::class.java)
                .also { given(it.validate(anyKotlin())).willReturn(Completable.complete()) }
    }
}

