package com.gocarry.grid.presentation.expense.add

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.gocarry.grid.core.action.AddExpense
import com.gocarry.grid.presentation.expense.add.AddExpenseViewModel.AddingState.*
import com.gocarry.grid.presentation.expense.add.AddExpenseViewModel.Payment
import com.gocarry.grid.test.extension.anyKotlin
import com.gocarry.grid.test.rule.RxImmediateSchedulerRule
import io.reactivex.Completable
import org.assertj.core.api.BDDAssertions.then
import org.junit.Rule
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito

class AddExpenseViewModelTest {

    @get:Rule
    val rxImmediateSchedulerRule = RxImmediateSchedulerRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val payments = listOf(Payment("userId", 300f), Payment("userId2", 50f))
    private val expense = AddExpenseViewModel.Expense("Nuevo pago", payments)
    private val addExpense = Mockito.mock(AddExpense::class.java)

    private val viewModel by lazy { AddExpenseViewModel("gridId", addExpense) }

    @Test
    fun `adding state should be success when expense is added`() {
        givenASuccessfulAddExpense()

        whenAddExpense()

        thenAddingStateIs(SUCCESS)
    }

    @Test
    fun `adding state should be failed when expense cant be added`() {
        givenAFailedAddExpense()

        whenAddExpense()

        thenAddingStateIs(FAILED)
    }

    @Test
    fun `adding state should be in progress when expense is been adding`() {
        givenAnInProgressAddExpense()

        whenAddExpense()

        thenAddingStateIs(IN_PROGRESS)
    }

    private fun givenAnInProgressAddExpense() {
        given(addExpense(anyKotlin())).willReturn(Completable.never())
    }

    private fun givenAFailedAddExpense() {
        given(addExpense(anyKotlin())).willReturn(Completable.error(RuntimeException()))
    }

    private fun givenASuccessfulAddExpense() {
        given(addExpense(anyKotlin())).willReturn(Completable.complete())
    }

    private fun whenAddExpense() {
        viewModel.addExpense(expense)
    }

    private fun thenAddingStateIs(state: AddExpenseViewModel.AddingState) {
        then(viewModel.addingStateLiveData.value).isEqualTo(state)
    }
}