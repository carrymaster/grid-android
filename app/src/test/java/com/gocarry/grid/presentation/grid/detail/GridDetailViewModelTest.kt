package com.gocarry.grid.presentation.grid.detail

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.gocarry.grid.core.action.FindGridDetail
import com.gocarry.grid.core.action.FindGridDetail.GridDetailResponse
import com.gocarry.grid.core.action.FindGridDetail.GridUserResponse
import com.gocarry.grid.presentation.grid.detail.GridDetailViewModel.GridDetail
import com.gocarry.grid.presentation.grid.detail.GridDetailViewModel.GridDetailState.*
import com.gocarry.grid.test.extension.anyKotlin
import com.gocarry.grid.test.rule.RxImmediateSchedulerRule
import io.reactivex.Single
import org.assertj.core.api.BDDAssertions.then
import org.junit.Rule
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito

class GridDetailViewModelTest {

    @get:Rule
    val rxImmediateSchedulerRule = RxImmediateSchedulerRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val gridDetailResponse = GridDetailResponse(listOf(GridUserResponse("12341", "Juan Carlos")))
    private val expectedGridDetail = GridDetail(listOf(GridDetailViewModel.GridUser("12341", "Juan Carlos")))
    private val findGridDetail = Mockito.mock(FindGridDetail::class.java)
    private lateinit var viewModel: GridDetailViewModel

    @Test
    fun `grid detail should be posted`() {
        givenASuccessfulFindGridDetail()

        whenViewModelIsCreated()

        thenGridDetailIsPosted()
    }

    @Test
    fun `state should be success when grid detail is found`() {
        givenASuccessfulFindGridDetail()

        whenViewModelIsCreated()

        thenGridDetailStateIs(SUCCESS)
    }

    @Test
    fun `state should be failed when grid detail cant be found`() {
        givenAFailedFindGridDetail()

        whenViewModelIsCreated()

        thenGridDetailStateIs(FAILED)
    }

    @Test
    fun `state should be in progress when grid is been finding`() {
        givenAnInProgressGridDetail()

        whenViewModelIsCreated()

        thenGridDetailStateIs(IN_PROGRESS)
    }

    private fun givenAnInProgressGridDetail() {
        given(findGridDetail(anyKotlin())).willReturn(Single.never())
    }

    private fun givenAFailedFindGridDetail() {
        given(findGridDetail(anyKotlin())).willReturn(Single.error(RuntimeException()))
    }

    private fun givenASuccessfulFindGridDetail() {
        given(findGridDetail(anyKotlin())).willReturn(Single.just(gridDetailResponse))
    }

    private fun whenViewModelIsCreated() {
        viewModel = GridDetailViewModel("gridId", findGridDetail)
    }

    private fun thenGridDetailStateIs(state: GridDetailViewModel.GridDetailState) {
        then(viewModel.gridDetailStateLiveData.value).isEqualTo(state)
    }

    private fun thenGridDetailIsPosted() {
        then(viewModel.gridDetailLiveData.value).isEqualTo(expectedGridDetail)
    }
}