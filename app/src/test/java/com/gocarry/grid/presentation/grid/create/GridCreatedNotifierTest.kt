package com.gocarry.grid.presentation.grid.create

import io.reactivex.observers.TestObserver
import org.junit.Test

class GridCreatedNotifierTest {

    private val gridCreatedNotifier = GridCreatedNotifier()
    private lateinit var gridCreatedTestObserver: TestObserver<GridCreatedNotifier.GridCreated>

    @Test
    fun `notify grid creation`() {
        whenNotify()
        thenGridCreatedIsNotified()
    }

    private fun whenNotify() {
        gridCreatedTestObserver = gridCreatedNotifier.gridCreationObserver.test()
        gridCreatedNotifier.notifyGridCreation()
    }

    private fun thenGridCreatedIsNotified() {
        gridCreatedTestObserver.assertValueCount(1)
        gridCreatedTestObserver.assertNotComplete()
        gridCreatedTestObserver.assertNoErrors()
    }

}