package com.gocarry.grid.presentation.grid.list

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.gocarry.grid.core.action.FindGrids
import com.gocarry.grid.presentation.grid.create.GridCreatedNotifier
import com.gocarry.grid.presentation.grid.list.GridsViewModel.GridsState.*
import com.gocarry.grid.presentation.user.add.UserAddedNotifier
import com.gocarry.grid.test.rule.RxImmediateSchedulerRule
import io.reactivex.Observable
import io.reactivex.Single
import org.assertj.core.api.BDDAssertions.then
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify


class GridsViewModelTest {

    @get:Rule
    val rxImmediateSchedulerRule = RxImmediateSchedulerRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val gridResponse = FindGrids.GridResponse("gridId", "best grid", 150f, 4)
    private val expectedGrid = GridsViewModel.Grid(gridResponse.id, gridResponse.description, gridResponse.userCount, gridResponse.totalSpent)

    private val findGrids = Mockito.mock(FindGrids::class.java)
    private val gridCreatedNotifier = Mockito.mock(GridCreatedNotifier::class.java)
    private val userAddedNotifier = Mockito.mock(UserAddedNotifier::class.java)

    private lateinit var viewModel: GridsViewModel

    @Before
    fun setUp() {
        given(findGrids()).willReturn(Single.never())
        given(gridCreatedNotifier.gridCreationObserver).willReturn(Observable.never())
        given(userAddedNotifier.userAddedObserver).willReturn(Observable.never())
    }

    @Test
    fun `post all grids`() {
        givenAGridResponse()

        whenViewModelIsCreated()

        thenGridIsReturned()
    }

    @Test
    fun `no grids to post`() {
        givenAnEmptyGridResponse()

        whenViewModelIsCreated()

        thenNoGridsAreReturned()
    }

    @Test
    fun `after a post grid state should be success`() {
        givenAGridResponse()

        whenViewModelIsCreated()

        thenGridStateIs(SUCCESS)
    }

    @Test
    fun `grid state should be error when find fails`() {
        givenAFailFindGrids()

        whenViewModelIsCreated()

        thenGridStateIs(ERROR)
    }

    @Test
    fun `grid state should be loading when find is working`() {
        givenANeverFinishFindGrids()

        whenViewModelIsCreated()

        thenGridStateIs(LOADING)
    }

    @Test
    fun `grids should be updated when a new grid is created`() {
        givenAGridResponse()
        givenNewGridIsCreated()

        whenViewModelIsCreated()

        thenGridsAreUpdatedTwice()
    }

    @Test
    fun `grids should be updated when a new user is added`() {
        givenAGridResponse()
        givenAUserAdded()

        whenViewModelIsCreated()

        thenGridsAreUpdatedTwice()
    }

    private fun givenAUserAdded() {
        given(userAddedNotifier.userAddedObserver).willReturn(Observable.just(UserAddedNotifier.UserAdded()))
    }

    private fun givenNewGridIsCreated() {
        given(gridCreatedNotifier.gridCreationObserver).willReturn(Observable.just(GridCreatedNotifier.GridCreated()))
    }

    private fun givenANeverFinishFindGrids() {
        given(findGrids()).willReturn(Single.never())
    }

    private fun givenAFailFindGrids() {
        given(findGrids()).willReturn(Single.error(RuntimeException()))
    }

    private fun givenAnEmptyGridResponse() {
        given(findGrids()).willReturn(Single.just(listOf()))
    }

    private fun givenAGridResponse() {
        given(findGrids()).willReturn(Single.just(listOf(gridResponse)))
    }

    private fun whenViewModelIsCreated() {
        createViewModel()
    }

    private fun thenGridsAreUpdatedTwice() {
        then(viewModel.gridsLiveData.value).containsExactlyInAnyOrder(expectedGrid)
        verify(findGrids, times(2)).invoke()
    }

    private fun thenGridIsReturned() {
        then(viewModel.gridsLiveData.value).containsExactlyInAnyOrder(expectedGrid)
        verify(findGrids, times(1)).invoke()
    }

    private fun thenNoGridsAreReturned() {
        then(viewModel.gridsLiveData.value).isEmpty()
    }

    private fun thenGridStateIs(expectedState: GridsViewModel.GridsState) {
        then(viewModel.gridsStateLiveData.value).isEqualTo(expectedState)
    }

    private fun createViewModel() {
        viewModel = GridsViewModel(findGrids, gridCreatedNotifier, userAddedNotifier)
    }
}
