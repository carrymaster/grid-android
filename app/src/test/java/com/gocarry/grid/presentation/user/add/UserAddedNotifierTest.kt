package com.gocarry.grid.presentation.user.add

import io.reactivex.observers.TestObserver
import org.junit.Test

class UserAddedNotifierTest {

    private val userAddedNotifier = UserAddedNotifier()
    private lateinit var userAddedTestObserver: TestObserver<UserAddedNotifier.UserAdded>

    @Test
    fun `notify user added`() {
        whenNotify()
        thenUserAddedIsNotified()
    }

    private fun whenNotify() {
        userAddedTestObserver = userAddedNotifier.userAddedObserver.test()
        userAddedNotifier.notifyUserAdded()
    }

    private fun thenUserAddedIsNotified() {
        userAddedTestObserver.assertValueCount(1)
        userAddedTestObserver.assertNotComplete()
        userAddedTestObserver.assertNoErrors()
    }

}