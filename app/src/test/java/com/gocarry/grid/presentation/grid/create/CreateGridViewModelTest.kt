package com.gocarry.grid.presentation.grid.create

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.gocarry.grid.core.action.CreateGrid
import com.gocarry.grid.login.core.action.FindLocalUser
import com.gocarry.grid.login.core.domain.LocalUser
import com.gocarry.grid.presentation.grid.create.CreateGridViewModel.CreateState.*
import com.gocarry.grid.test.extension.anyKotlin
import com.gocarry.grid.test.rule.RxImmediateSchedulerRule
import io.reactivex.Completable
import org.assertj.core.api.BDDAssertions.then
import org.junit.Rule
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito
import org.mockito.Mockito.verify

class CreateGridViewModelTest {

    @get:Rule
    val rxImmediateSchedulerRule = RxImmediateSchedulerRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val gridToCreate = CreateGridViewModel.GridToCreate("the best grid")
    private val localUser = LocalUser("local_user_id", "User Name")

    private val createGrid = Mockito.mock(CreateGrid::class.java)
    private val gridCreationNotifier = Mockito.mock(GridCreatedNotifier::class.java)
    private val findLocalUser = createFindLocalUser()

    private val viewModel by lazy { CreateGridViewModel(createGrid, findLocalUser, gridCreationNotifier) }

    @Test
    fun `create state should be success when creation completes`() {
        givenASuccessfulGridCreation()

        whenCreateIt()

        thenCreateStateIs(SUCCEED)
    }

    @Test
    fun `create state should be failed when creation fails`() {
        givenAFailedGridCreation()

        whenCreateIt()

        thenCreateStateIs(FAILED)
    }

    @Test
    fun `create state should be in progress when creation is working`() {
        givenANeverEndGridCreation()

        whenCreateIt()

        thenCreateStateIs(IN_PROGRESS)
    }

    @Test
    fun `grid creation should be notified`() {
        givenASuccessfulGridCreation()

        whenCreateIt()

        thenCreationIsNotified()
    }

    private fun givenANeverEndGridCreation() {
        given(createGrid(anyKotlin())).willReturn(Completable.never())
    }

    private fun givenAFailedGridCreation() {
        given(createGrid(anyKotlin())).willReturn(Completable.error(RuntimeException()))
    }

    private fun givenASuccessfulGridCreation() {
        given(createGrid(anyKotlin())).willReturn(Completable.complete())
    }

    private fun whenCreateIt() {
        viewModel.createGrid(gridToCreate)
    }

    private fun thenCreateStateIs(expectedCreateState: CreateGridViewModel.CreateState) {
        then(viewModel.createStateLiveData.value).isEqualTo(expectedCreateState)
    }

    private fun thenCreationIsNotified() {
        verify(gridCreationNotifier).notifyGridCreation()
    }

    private fun createFindLocalUser(): FindLocalUser {
        return Mockito.mock(FindLocalUser::class.java)
                .also { given(it.invoke()).willReturn(localUser) }
    }
}