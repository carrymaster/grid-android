package com.gocarry.grid.presentation.user.add

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.gocarry.grid.core.action.AddGridUser
import com.gocarry.grid.presentation.user.add.AddGridUserViewModel.AdditionState.*
import com.gocarry.grid.test.extension.anyKotlin
import com.gocarry.grid.test.rule.RxImmediateSchedulerRule
import io.reactivex.Completable
import org.assertj.core.api.BDDAssertions.then
import org.junit.Rule
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito.*

class AddGridUserViewModelTest {

    @get:Rule
    val rxImmediateSchedulerRule = RxImmediateSchedulerRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val gridId = "gridId"
    private val userName = "Amparo"

    private val addGridUser = mock(AddGridUser::class.java)
    private val userAddedNotifier = mock(UserAddedNotifier::class.java)

    private lateinit var viewModel: AddGridUserViewModel

    @Test
    fun `addition state should be success when user is added`() {
        givenASuccessfulCreation()
        givenAViewModel()

        whenAddUser()

        thenAdditionStateIs(SUCCESS)
    }

    @Test
    fun `addition state should be failed when user could not be added`() {
        givenAFailedCreation()
        givenAViewModel()

        whenAddUser()

        thenAdditionStateIs(FAILED)
    }

    @Test
    fun `addition state should be in progress when user are being adding`() {
        givenACreationInProgress()
        givenAViewModel()

        whenAddUser()

        thenAdditionStateIs(IN_PROGRESS)
    }

    @Test
    fun `addition should be notified when user is added`() {
        givenASuccessfulCreation()
        givenAViewModel()

        whenAddUser()

        thenAdditionIsNotified()
    }

    private fun givenACreationInProgress() {
        given(addGridUser(anyKotlin())).willReturn(Completable.never())
    }

    private fun givenAFailedCreation() {
        given(addGridUser(anyKotlin())).willReturn(Completable.error(RuntimeException()))
    }

    private fun givenASuccessfulCreation() {
        given(addGridUser(anyKotlin())).willReturn(Completable.complete())
    }

    private fun givenAViewModel() {
        viewModel = AddGridUserViewModel(gridId, addGridUser, userAddedNotifier)
    }

    private fun whenAddUser() {
        viewModel.addUser(AddGridUserViewModel.GridUser(userName))
    }

    private fun thenAdditionStateIs(expectedState: AddGridUserViewModel.AdditionState) {
        then(viewModel.additionStateLiveData.value).isEqualTo(expectedState)
    }

    private fun thenAdditionIsNotified() {
        verify(userAddedNotifier, times(1)).notifyUserAdded()
    }
}