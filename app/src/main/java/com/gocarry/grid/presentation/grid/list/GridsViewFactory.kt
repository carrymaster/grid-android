package com.gocarry.grid.presentation.grid.list

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.gocarry.grid.utils.recyclerview.RecyclerViewFactory
import com.gocarry.thegrid.R

class GridsViewFactory(private val context: Context) : RecyclerViewFactory {

    private val layoutInflater by lazy { LayoutInflater.from(context) }

    override fun getViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            GRID_ITEM -> createGridItem(parent)
            else -> throw IllegalArgumentException("$viewType is not a valid view type for ${this.javaClass.simpleName}")
        }
    }

    private fun createGridItem(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = layoutInflater.inflate(R.layout.view_grid_item, parent, false)
        return GridItem.ViewHolder(view)
    }

    companion object {
        const val GRID_ITEM = 0
    }
}