package com.gocarry.grid.presentation.grid.list

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gocarry.grid.presentation.grid.detail.GridDetailActivity
import com.gocarry.grid.presentation.grid.list.GridsViewModel.Grid
import com.gocarry.thegrid.R
import kotlinx.android.synthetic.main.fragment_grids.*

class GridsFragment : Fragment() {

    private val viewModel by lazy { GridsViewModelFactory.create(this) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_grids, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.gridsLiveData.observe(this, Observer { grids ->
            grids?.also { recyclerView.show(it) { startGridDetailActivity(it) } }
        })
    }

    private fun startGridDetailActivity(grid: Grid) {
        context?.also { startActivity(GridDetailActivity.newIntent(it, grid.id)) }
    }

    companion object {
        fun newFragment() = GridsFragment()
    }
}