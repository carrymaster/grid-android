package com.gocarry.grid.presentation.grid.list

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.Fragment
import com.gocarry.grid.presentation.config.PresentationModule
import com.gocarry.grid.room.config.RoomModule

@Suppress("UNCHECKED_CAST")
object GridsViewModelFactory {

    private val factory get() = object : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return GridsViewModel(RoomModule.roomFindGrids, PresentationModule.gridCreatedNotifier, PresentationModule.userAddedNotifier) as T
        }
    }

    fun create(fragment: Fragment): GridsViewModel {
        return ViewModelProviders.of(fragment, factory).get(GridsViewModel::class.java)
    }
}