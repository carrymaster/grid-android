package com.gocarry.grid.presentation.grid.list

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.gocarry.grid.utils.recyclerview.RecyclerViewItem
import com.gocarry.thegrid.R

class GridItem(private val grid: GridsViewModel.Grid,
               private val onItemClicked: (GridsViewModel.Grid) -> Unit) : RecyclerViewItem {

    override val itemViewType = GridsViewFactory.GRID_ITEM

    override fun bind(viewHolder: RecyclerView.ViewHolder) {
        viewHolder as GridItem.ViewHolder
        viewHolder.apply {
            descriptionTextView.text = grid.description
            totalSpentTextView.text = "$${grid.totalSpent}"
            userCountTextView.text = grid.userCount.toString()
            itemView.setOnClickListener { onItemClicked(grid) }
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val descriptionTextView = view.findViewById<TextView>(R.id.descriptionTextView)!!
        val totalSpentTextView = view.findViewById<TextView>(R.id.totalSpentTextView)!!
        val userCountTextView = view.findViewById<TextView>(R.id.userCountTextView)!!
    }
}