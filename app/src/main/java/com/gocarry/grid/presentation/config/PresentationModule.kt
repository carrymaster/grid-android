package com.gocarry.grid.presentation.config

import com.gocarry.grid.presentation.grid.create.GridCreatedNotifier
import com.gocarry.grid.presentation.user.add.UserAddedNotifier
import com.gocarry.grid.utils.InstanceCache

object PresentationModule {

    val gridCreatedNotifier get() = InstanceCache.instance(GridCreatedNotifier::class) { GridCreatedNotifier() }

    val userAddedNotifier get() = InstanceCache.instance(UserAddedNotifier::class) { UserAddedNotifier() }
}