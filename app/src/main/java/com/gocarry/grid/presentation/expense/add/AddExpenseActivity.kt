package com.gocarry.grid.presentation.expense.add

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.gocarry.grid.presentation.grid.detail.GridDetailViewModel.GridUser
import com.gocarry.thegrid.R
import kotlinx.android.synthetic.main.activity_add_grid_user.*

class AddExpenseActivity : AppCompatActivity() {

    private val gridUsers get() = intent.extras.getSerializable(GRID_USERS_EXTRA) as ArrayList<GridUser>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_expense)
        configureToolbar()
        addMainFragment()
    }

    private fun addMainFragment() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.mainFrameLayout, AddExpenseFragment.newFragment(gridUsers))
                .commit()
    }

    private fun configureToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    companion object {
        private const val GRID_USERS_EXTRA = "grid_users"
        fun newIntent(context: Context, gridUsers: ArrayList<GridUser>) =
                Intent(context, AddExpenseActivity::class.java)
                        .apply {
                            putExtra(GRID_USERS_EXTRA, gridUsers)
                            flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                        }
    }
}