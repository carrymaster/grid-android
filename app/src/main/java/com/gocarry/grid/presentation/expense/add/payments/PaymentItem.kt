package com.gocarry.grid.presentation.expense.add.payments

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.gocarry.grid.presentation.expense.add.payments.PaymentsViewFactory.Companion.PAYMENT_ITEM
import com.gocarry.grid.presentation.grid.detail.GridDetailViewModel
import com.gocarry.grid.utils.recyclerview.RecyclerViewItem
import com.gocarry.thegrid.R

class PaymentItem(private val gridUser: GridDetailViewModel.GridUser) : RecyclerViewItem {

    override val itemViewType: Int
        get() = PAYMENT_ITEM

    override fun bind(viewHolder: RecyclerView.ViewHolder) {
        viewHolder as PaymentItem.ViewHolder
        viewHolder.userNameTextView.text = gridUser.userName
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val userNameTextView = view.findViewById<TextView>(R.id.userNameTextView)!!
    }
}