package com.gocarry.grid.presentation.grid.detail

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.FragmentActivity
import com.gocarry.grid.room.config.RoomModule

object GridDetailViewModelFactory {

    fun create(activity: FragmentActivity, gridId: String): GridDetailViewModel {
        return ViewModelProviders.of(activity, createFactory(gridId)).get(GridDetailViewModel::class.java)
    }

    private fun createFactory(gridId: String): ViewModelProvider.Factory {
        return object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return GridDetailViewModel(gridId, RoomModule.roomFindGridDetail) as T
            }
        }
    }
}