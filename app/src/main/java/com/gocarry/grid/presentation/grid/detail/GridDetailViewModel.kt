package com.gocarry.grid.presentation.grid.detail

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.gocarry.grid.core.action.FindGridDetail
import com.gocarry.grid.presentation.grid.detail.GridDetailViewModel.GridDetailState.*
import com.gocarry.grid.utils.extension.onDefaultSchedulers
import io.reactivex.rxkotlin.subscribeBy
import java.io.Serializable

class GridDetailViewModel(private val gridId: String,
                          private val findGridDetail: FindGridDetail) : ViewModel() {

    val gridDetailLiveData: LiveData<GridDetail> get() = gridDetailMutableLiveData
    val gridDetailStateLiveData: LiveData<GridDetailState> get() = gridDetailStateMutableLiveData
    private val gridDetailMutableLiveData = MutableLiveData<GridDetail>()
    private val gridDetailStateMutableLiveData = MutableLiveData<GridDetailState>()

    init {
        findGridDetail()
    }

    private fun findGridDetail() {
        findGridDetail(gridId)
                .onDefaultSchedulers()
                .doOnSubscribe { postGridDetailState(IN_PROGRESS) }
                .doOnError { postGridDetailState(FAILED) }
                .subscribeBy { onGridDetailFound(it) }
    }

    private fun onGridDetailFound(gridDetailResponse: FindGridDetail.GridDetailResponse) {
        postGridDetailState(SUCCESS)
        postGridDetail(createGridDetail(gridDetailResponse))
    }

    private fun postGridDetailState(state: GridDetailState) {
        gridDetailStateMutableLiveData.postValue(state)
    }

    private fun postGridDetail(gridDetail: GridDetail) {
        gridDetailMutableLiveData.postValue(gridDetail)
    }

    private fun createGridDetail(gridDetailResponse: FindGridDetail.GridDetailResponse): GridDetail =
            GridDetail(createGridUsers(gridDetailResponse))

    private fun createGridUsers(gridDetailResponse: FindGridDetail.GridDetailResponse) =
            gridDetailResponse.users.map { GridUser(it.id, it.userName) }

    data class GridDetail(val users: List<GridUser>)

    data class GridUser(val userId: String, val userName: String) : Serializable

    enum class GridDetailState {
        SUCCESS, FAILED, IN_PROGRESS
    }
}