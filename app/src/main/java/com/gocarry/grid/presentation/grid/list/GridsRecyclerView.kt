package com.gocarry.grid.presentation.grid.list

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import com.gocarry.grid.presentation.grid.list.GridsViewModel.Grid
import com.gocarry.grid.utils.recyclerview.RecyclerViewAdapter

class GridsRecyclerView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) : RecyclerView(context, attrs, defStyle) {

    private val recyclerViewAdapter get() = adapter as RecyclerViewAdapter

    init {
        layoutManager = LinearLayoutManager(context, VERTICAL, false)
        adapter = RecyclerViewAdapter(GridsViewFactory(context))
    }

    fun show(grids: List<Grid>, onItemClicked: (GridsViewModel.Grid) -> Unit) {
        grids.map { GridItem(it, onItemClicked) }.also { recyclerViewAdapter.updateWith(it) }
    }
}