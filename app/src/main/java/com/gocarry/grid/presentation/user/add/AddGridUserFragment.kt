package com.gocarry.grid.presentation.user.add

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.gocarry.grid.presentation.user.add.AddGridUserViewModel.AdditionState.FAILED
import com.gocarry.grid.presentation.user.add.AddGridUserViewModel.AdditionState.SUCCESS
import com.gocarry.grid.presentation.user.add.AddGridUserViewModel.GridUser
import com.gocarry.thegrid.R
import kotlinx.android.synthetic.main.fragment_add_grid_user.*

class AddGridUserFragment : Fragment() {

    private val gridId get() = arguments!!.getString(GRID_ID_ARGUMENT)
    private val viewModel by lazy { AddGridUserViewModelFactory.create(this, gridId) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_grid_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.additionStateLiveData.observe(this, Observer {
            when (it) {
                SUCCESS -> onUserAdditionSuccess()
                FAILED -> onUserAdditionFailed()
                else -> {
                }
            }
        })
        addButton.setOnClickListener { addGridUser() }
    }

    private fun onUserAdditionSuccess() {
        context?.also { Toast.makeText(it, "Agregado!", Toast.LENGTH_LONG).show() }
        activity?.finish()
    }

    private fun onUserAdditionFailed() {
        context?.also { Toast.makeText(it, "Ocurrio un error", Toast.LENGTH_LONG).show() }
    }

    private fun addGridUser() {
        val userName = nameEditText.text.toString()
        viewModel.addUser(GridUser(userName))
    }

    companion object {
        private const val GRID_ID_ARGUMENT = "grid_id"

        fun newFragment(gridId: String): AddGridUserFragment {
            return AddGridUserFragment()
                    .apply { arguments = createBundleWith(gridId) }
        }

        private fun createBundleWith(gridId: String) =
                Bundle().apply { putString(GRID_ID_ARGUMENT, gridId) }
    }
}