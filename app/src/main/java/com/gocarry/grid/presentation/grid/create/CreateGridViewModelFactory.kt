package com.gocarry.grid.presentation.grid.create

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.Fragment
import com.gocarry.grid.login.core.config.LoginModule
import com.gocarry.grid.presentation.config.PresentationModule
import com.gocarry.grid.room.config.RoomModule

object CreateGridViewModelFactory {

    private val factory = object : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return CreateGridViewModel(RoomModule.roomCreateGrid, LoginModule.dummyFindLocalUser, PresentationModule.gridCreatedNotifier ) as T
        }
    }

    fun create(fragment: Fragment): CreateGridViewModel {
        return ViewModelProviders.of(fragment, factory).get(CreateGridViewModel::class.java)
    }
}