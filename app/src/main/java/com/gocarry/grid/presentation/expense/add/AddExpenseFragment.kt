package com.gocarry.grid.presentation.expense.add

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import com.gocarry.grid.presentation.grid.detail.GridDetailViewModel
import com.gocarry.thegrid.R
import kotlinx.android.synthetic.main.fragment_add_expense.*


class AddExpenseFragment : Fragment() {

    private val gridUsers get() = (arguments!!.getSerializable(GRID_USERS_ARG) as ArrayList<GridDetailViewModel.GridUser>).toList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_expense, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        paymentsRecyclerView.show(gridUsers)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_add_expense, menu)
    }

    companion object {
        private const val GRID_USERS_ARG = "grid_users"
        fun newFragment(gridUsers: ArrayList<GridDetailViewModel.GridUser>): AddExpenseFragment {
            return AddExpenseFragment().apply { arguments = createArguments(gridUsers) }
        }

        private fun createArguments(gridUsers: ArrayList<GridDetailViewModel.GridUser>) =
                Bundle().apply { putSerializable(GRID_USERS_ARG, gridUsers) }
    }
}