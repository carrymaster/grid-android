package com.gocarry.grid.presentation.grid.list

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.gocarry.grid.presentation.grid.create.CreateGridActivity
import com.gocarry.thegrid.R
import kotlinx.android.synthetic.main.activity_grids.*

class GridsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grids)
        setSupportActionBar(toolbar)
        addGridsFragment()
        addExpenseFloatingActionButton.setOnClickListener { startActivity(CreateGridActivity.newIntent(this)) }
    }

    private fun addGridsFragment() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.mainFrameLayout, GridsFragment.newFragment())
                .commit()
    }
}