package com.gocarry.grid.presentation.grid.create

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.gocarry.grid.utils.extension.navigateUp
import com.gocarry.thegrid.R
import kotlinx.android.synthetic.main.activity_create_grid.*

class CreateGridActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_grid)
        configureToolbar()
        supportFragmentManager.beginTransaction()
                .replace(R.id.mainFrameLayout, CreateGridFragment.newFragment())
                .commit()
    }

    private fun configureToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> navigateUp().let { true }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        fun newIntent(context: Context) =
                Intent(context, CreateGridActivity::class.java)
                        .apply {
                            flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                        }
    }
}