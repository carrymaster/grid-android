package com.gocarry.grid.presentation.user.add

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.Fragment
import com.gocarry.grid.presentation.config.PresentationModule
import com.gocarry.grid.room.config.RoomModule

@Suppress("UNCHECKED_CAST")
object AddGridUserViewModelFactory {

    fun create(fragment: Fragment, gridId: String): AddGridUserViewModel {
        return ViewModelProviders.of(fragment, createFactory(gridId)).get(AddGridUserViewModel::class.java)
    }

    private fun createFactory(gridId: String) =
            object : ViewModelProvider.Factory {
                override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                    return AddGridUserViewModel(gridId, RoomModule.roomAddGridUser, PresentationModule.userAddedNotifier) as T
                }
            }
}