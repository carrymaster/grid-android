package com.gocarry.grid.presentation.user.add

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.gocarry.thegrid.R
import kotlinx.android.synthetic.main.activity_add_grid_user.*

class AddGridUserActivity : AppCompatActivity() {

    private val gridId
        get() = intent.extras.getString(GRID_ID_EXTRA)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_grid_user)
        configureToolbar()
        addGridDetailFragment()
    }

    private fun addGridDetailFragment() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.mainFrameLayout, AddGridUserFragment.newFragment(gridId))
                .commit()
    }

    private fun configureToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    companion object {
        private const val GRID_ID_EXTRA = "grid_id"
        fun newIntent(context: Context, gridId: String) =
                Intent(context, AddGridUserActivity::class.java)
                        .apply {
                            putExtra(GRID_ID_EXTRA, gridId)
                            flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                        }
    }
}