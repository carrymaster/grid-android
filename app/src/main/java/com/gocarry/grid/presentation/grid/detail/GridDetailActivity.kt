package com.gocarry.grid.presentation.grid.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.gocarry.grid.presentation.expense.add.AddExpenseActivity
import com.gocarry.grid.presentation.user.add.AddGridUserActivity
import com.gocarry.grid.utils.extension.navigateUp
import com.gocarry.thegrid.R
import kotlinx.android.synthetic.main.activity_grid_detail.*


class GridDetailActivity : AppCompatActivity() {

    private val gridId
        get() = intent.extras.getString(GRID_ID_EXTRA)

    private val viewModel by lazy { GridDetailViewModelFactory.create(this, gridId) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_detail)
        configureToolbar()
        configureAddExpenseButton()
        addGridDetailFragment()
    }

    private fun configureToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun configureAddExpenseButton() {
        addExpenseFloatingActionButton.setOnClickListener {
            val gridUser = getGridUsers()
            getGridUsers()?.also {
                startActivity(AddExpenseActivity.newIntent(this, ArrayList(it)))
            }
        }
    }

    fun getGridUsers() = viewModel.gridDetailLiveData.value?.users

    private fun addGridDetailFragment() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.mainFrameLayout, GridDetailFragment.newFragment())
                .commit()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return menuInflater.inflate(R.menu.menu_grid_detail, menu)
                .let { true }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> navigateUp().let { true }
            R.id.action_add_grid_user -> startActivity(AddGridUserActivity.newIntent(this, gridId)).let { true }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        private const val GRID_ID_EXTRA = "grid_id"

        fun newIntent(context: Context, gridId: String) =
                Intent(context, GridDetailActivity::class.java)
                        .apply {
                            putExtra(GRID_ID_EXTRA, gridId)
                            flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                        }
    }
}