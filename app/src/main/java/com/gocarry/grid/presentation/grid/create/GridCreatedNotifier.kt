package com.gocarry.grid.presentation.grid.create

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class GridCreatedNotifier {

    val gridCreationObserver get() =  gridCreationSubject as Observable<GridCreated>
    private val gridCreationSubject = PublishSubject.create<GridCreated>()

    fun notifyGridCreation() {
        gridCreationSubject.onNext(GridCreated())
    }

    class GridCreated
}