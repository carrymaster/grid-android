package com.gocarry.grid.presentation.grid.create

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.gocarry.grid.presentation.grid.create.CreateGridViewModel.CreateState.FAILED
import com.gocarry.grid.presentation.grid.create.CreateGridViewModel.CreateState.SUCCEED
import com.gocarry.thegrid.R
import kotlinx.android.synthetic.main.fragment_create_grid.*

class CreateGridFragment : Fragment() {

    private val viewModel by lazy { CreateGridViewModelFactory.create(this) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_create_grid, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.createStateLiveData.observe(this, Observer {
            when (it) {
                SUCCEED -> onCreateSuccess()
                FAILED -> onCreateFailed()
                else -> {
                }
            }
        })

        createButton.setOnClickListener {
            viewModel.createGrid(createGridToCreate())
        }
    }

    private fun onCreateSuccess() {
        Toast.makeText(context, "Grilla creada!", Toast.LENGTH_LONG).show()
        activity!!.finish()
    }

    private fun onCreateFailed() {
        Toast.makeText(context, "Ocurrio un error", Toast.LENGTH_LONG).show()
    }

    private fun createGridToCreate() = CreateGridViewModel.GridToCreate(descriptionEditText.text.toString())

    companion object {
        fun newFragment() = CreateGridFragment()
    }
}