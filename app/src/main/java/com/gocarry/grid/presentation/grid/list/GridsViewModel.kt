package com.gocarry.grid.presentation.grid.list

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.gocarry.grid.core.action.FindGrids
import com.gocarry.grid.presentation.grid.create.GridCreatedNotifier
import com.gocarry.grid.presentation.user.add.UserAddedNotifier
import com.gocarry.grid.utils.extension.onDefaultSchedulers
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class GridsViewModel(private val findGrids: FindGrids,
                     private val gridCreatedNotifier: GridCreatedNotifier,
                     private val userAddedNotifier: UserAddedNotifier) : ViewModel() {

    val gridsLiveData: LiveData<List<Grid>> get() = gridsMutableLiveData
    val gridsStateLiveData: LiveData<GridsState> get() = gridsStateMutableLiveData

    private val gridsMutableLiveData = MutableLiveData<List<Grid>>()
    private val gridsStateMutableLiveData = MutableLiveData<GridsState>()
    private val compositeDisposable = CompositeDisposable()

    init {
        findGridsAndPostIt()
        refreshOnGridUpdate()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    private fun findGridsAndPostIt() {
        findGrids()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .doOnSubscribe { postGridState(GridsState.LOADING) }
                .map { createGrids(it) }
                .subscribeBy(onSuccess = { onFindGridsSucceed(it) }, onError = { onFindGridsFails(it) })
                .addTo(compositeDisposable)
    }

    private fun refreshOnGridUpdate() {
        gridCreatedNotifier.gridCreationObserver
                .onDefaultSchedulers()
                .subscribeBy { findGridsAndPostIt() }

        userAddedNotifier.userAddedObserver
                .onDefaultSchedulers()
                .subscribeBy { findGridsAndPostIt() }
    }

    private fun onFindGridsSucceed(grids: List<Grid>) {
        postGrids(grids)
        postGridState(GridsState.SUCCESS)
    }

    private fun onFindGridsFails(throwable: Throwable) {
        postGridState(GridsState.ERROR)
    }

    private fun postGrids(grids: List<Grid>) {
        gridsMutableLiveData.postValue(grids)
    }

    private fun postGridState(state: GridsState) {
        gridsStateMutableLiveData.postValue(state)
    }

    private fun createGrids(gridResponses: List<FindGrids.GridResponse>) =
            gridResponses.map { Grid(it.id, it.description, it.userCount, it.totalSpent) }

    data class Grid(val id: String, val description: String, val userCount: Int, val totalSpent: Float)

    enum class GridsState {
        SUCCESS, ERROR, LOADING
    }
}