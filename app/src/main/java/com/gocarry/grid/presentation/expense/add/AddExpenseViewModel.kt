package com.gocarry.grid.presentation.expense.add

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.gocarry.grid.core.action.AddExpense
import com.gocarry.grid.presentation.expense.add.AddExpenseViewModel.AddingState.*
import com.gocarry.grid.utils.extension.onDefaultSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class AddExpenseViewModel(private val gridId: String,
                          private val addExpense: AddExpense) : ViewModel() {

    val addingStateLiveData: LiveData<AddingState> get() = addingStateMutableLiveData

    private val addingStateMutableLiveData = MutableLiveData<AddingState>()
    private val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun addExpense(expense: Expense) {
        addExpense(createExpenseToAdd(expense))
                .onDefaultSchedulers()
                .doOnSubscribe { postAddingState(IN_PROGRESS) }
                .doOnError { postAddingState(FAILED) }
                .subscribeBy { postAddingState(SUCCESS) }
                .addTo(compositeDisposable)
    }

    private fun postAddingState(state: AddingState) {
        addingStateMutableLiveData.postValue(state)
    }

    private fun createExpenseToAdd(expense: Expense) =
            AddExpense.ExpenseToAdd(gridId, createPaymentsToAdd(expense), expense.description)

    private fun createPaymentsToAdd(expense: Expense) =
            expense.payments.map { AddExpense.ExpenseToAdd.Payment(it.userId, it.amount) }

    data class Expense(val description: String, val payments: List<Payment>)

    data class Payment(val userId: String, val amount: Float)

    enum class AddingState {
        SUCCESS, FAILED, IN_PROGRESS
    }
}