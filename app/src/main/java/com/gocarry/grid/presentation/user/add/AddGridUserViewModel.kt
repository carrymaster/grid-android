package com.gocarry.grid.presentation.user.add

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.gocarry.grid.core.action.AddGridUser
import com.gocarry.grid.presentation.user.add.AddGridUserViewModel.AdditionState.*
import com.gocarry.grid.utils.extension.onDefaultSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import org.joda.time.DateTime
import java.util.*

class AddGridUserViewModel(private val gridId: String,
                           private val addGridUser: AddGridUser,
                           private val userAddedNotifier: UserAddedNotifier) : ViewModel() {

    val additionStateLiveData: LiveData<AdditionState> get() = additionStateMutableLiveData
    private val additionStateMutableLiveData = MutableLiveData<AdditionState>()
    private val compositeDisposable = CompositeDisposable()

    fun addUser(gridUser: AddGridUserViewModel.GridUser) {
        addGridUser(gridUser.let { AddGridUser.GridUserToAdd(gridId, createRandomUserId(), it.userName) })
                .onDefaultSchedulers()
                .doOnSubscribe { postAdditionState(IN_PROGRESS) }
                .doOnError { postAdditionState(FAILED) }
                .subscribeBy(onComplete = { onUserAdded() })
                .addTo(compositeDisposable)
    }

    private fun onUserAdded() {
        postAdditionState(SUCCESS)
        userAddedNotifier.notifyUserAdded()
    }

    private fun createRandomUserId(): String {
        return Random(DateTime.now().millis).nextInt().toString()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    private fun postAdditionState(additionState: AdditionState) {
        additionStateMutableLiveData.postValue(additionState)
    }

    data class GridUser(val userName: String)

    enum class AdditionState {
        SUCCESS, FAILED, IN_PROGRESS
    }
}