package com.gocarry.grid.presentation.expense.add.payments

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.gocarry.grid.utils.recyclerview.RecyclerViewFactory
import com.gocarry.thegrid.R

class PaymentsViewFactory(private val context: Context) : RecyclerViewFactory {

    override fun getViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType) {
            PAYMENT_ITEM -> createPaymentItem(parent)
            else -> throw IllegalArgumentException("$viewType is not a valid view type for PaymentViewFactory")
        }
    }

    private fun createPaymentItem(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.view_payment_item, parent, false)
        return PaymentItem.ViewHolder(view)
    }

    companion object {
        const val PAYMENT_ITEM = 0
    }
}