package com.gocarry.grid.presentation.grid.detail

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gocarry.thegrid.R

class GridDetailFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_grid_detail, container, false)
    }

    companion object {
        fun newFragment() = GridDetailFragment()
    }
}