package com.gocarry.grid.presentation.expense.add.payments

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import com.gocarry.grid.presentation.grid.detail.GridDetailViewModel
import com.gocarry.grid.utils.recyclerview.RecyclerViewAdapter

class PaymentsRecylclerView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) : RecyclerView(context, attrs, defStyle) {

    private val customAdapter = RecyclerViewAdapter(PaymentsViewFactory(context))

    init {
        layoutManager = LinearLayoutManager(context, VERTICAL, false)
        adapter = customAdapter
    }

    fun show(gridUsers: List<GridDetailViewModel.GridUser>) {
        val items = gridUsers.map { PaymentItem(it) }
        customAdapter.updateWith(items)
    }
}