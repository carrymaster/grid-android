package com.gocarry.grid.presentation.grid.create

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.gocarry.grid.core.action.CreateGrid
import com.gocarry.grid.login.core.action.FindLocalUser
import com.gocarry.grid.presentation.grid.create.CreateGridViewModel.CreateState.*
import com.gocarry.grid.utils.extension.onDefaultSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class CreateGridViewModel(private val createGrid: CreateGrid,
                          private val findLocalUser: FindLocalUser,
                          private val gridCreationNotifier: GridCreatedNotifier) : ViewModel() {

    val createStateLiveData: LiveData<CreateState> get() = createStateMutableLiveData

    private val createStateMutableLiveData = MutableLiveData<CreateState>()

    private val compositeDisposable = CompositeDisposable()

    fun createGrid(gridToCreate: GridToCreate) {
        createGrid(CreateGrid.GridToCreate(gridToCreate.description, createCreator()))
                .onDefaultSchedulers()
                .doOnSubscribe { postCreateState(IN_PROGRESS) }
                .subscribeBy(onComplete = { onCreateGridCompleted() }, onError = { onCreateGridError() })
                .addTo(compositeDisposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    private fun onCreateGridCompleted() {
        postCreateState(SUCCEED)
        gridCreationNotifier.notifyGridCreation()
    }

    private fun onCreateGridError() {
        postCreateState(FAILED)
    }

    private fun postCreateState(createState: CreateState) {
        createStateMutableLiveData.postValue(createState)
    }

    private fun createCreator(): CreateGrid.GridToCreate.Creator {
        return findLocalUser().let { CreateGrid.GridToCreate.Creator(it.id, it.userName) }
    }

    data class GridToCreate(val description: String)

    enum class CreateState {
        SUCCEED, FAILED, IN_PROGRESS
    }
}
