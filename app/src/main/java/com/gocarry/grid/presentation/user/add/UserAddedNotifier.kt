package com.gocarry.grid.presentation.user.add

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class UserAddedNotifier {

    val userAddedObserver get() =  gridCreationSubject as Observable<UserAdded>
    private val gridCreationSubject = PublishSubject.create<UserAdded>()

    fun notifyUserAdded() {
        gridCreationSubject.onNext(UserAdded())
    }

    class UserAdded
}