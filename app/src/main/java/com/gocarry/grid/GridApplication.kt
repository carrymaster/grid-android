package com.gocarry.grid

import android.app.Application
import android.content.Context
import android.util.Log
import com.gocarry.grid.utils.InstanceCache
import io.reactivex.plugins.RxJavaPlugins

class GridApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        InstanceCache.instance(Context::class) { applicationContext }
        RxJavaPlugins.setErrorHandler { Log.e("DefaultErrorHandler", it.message, it) }
    }

}