package com.gocarry.grid.login.core.domain

data class LocalUser(val id: String, val userName: String)