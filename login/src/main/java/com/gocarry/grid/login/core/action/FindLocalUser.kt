package com.gocarry.grid.login.core.action

import com.gocarry.grid.login.core.domain.LocalUser

class FindLocalUser {
    operator fun invoke(): LocalUser {
        return LocalUser("local_user_id", "Local User Name")
    }
}