package com.gocarry.grid.login.core.config

import com.gocarry.grid.login.core.action.FindLocalUser

object LoginModule {
    val dummyFindLocalUser get() = FindLocalUser()
}