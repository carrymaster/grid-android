package com.gocarry.grid.test.core.domain.factory

import com.gocarry.grid.core.domain.Balance
import com.gocarry.grid.core.domain.UserBalance
import com.gocarry.grid.test.core.domain.factory.FactoryDefaultValues.gridUserId

object BalanceFactory {

    fun create(userBalanceSet: Set<UserBalance> = FactoryDefaultValues.userBalanceSet, amount: Float = FactoryDefaultValues.balanceAmount): Balance {
        return Balance.create(userBalanceSet, amount)
    }

    fun createInitial(userIdSet: Set<String> = setOf(gridUserId(1), gridUserId(2))): Balance {
        return Balance.createInitial(userIdSet)
    }
}