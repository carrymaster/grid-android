package com.gocarry.grid.test.core.domain.factory

import com.gocarry.grid.core.domain.Expense
import com.gocarry.grid.core.domain.Payment
import com.gocarry.grid.test.core.domain.factory.FactoryDefaultValues.expenseDescription

object ExpenseFactory {

    fun create(id: String = FactoryDefaultValues.expenseId(), gridId: String = FactoryDefaultValues.gridId, paymentList: List<Payment> = FactoryDefaultValues.paymentList, description: String = expenseDescription): Expense {
        return Expense.create(id, gridId, paymentList, description)
    }
}