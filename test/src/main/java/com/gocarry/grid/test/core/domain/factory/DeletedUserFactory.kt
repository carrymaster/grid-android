package com.gocarry.grid.test.core.domain.factory

import com.gocarry.grid.core.domain.DeletedUser

object DeletedUserFactory {
    fun create(id: String = FactoryDefaultValues.gridUserId(), gridId: String = FactoryDefaultValues.gridId): DeletedUser {
        return DeletedUser(id, gridId)
    }
}