package com.gocarry.grid.test.core.domain.factory

import com.gocarry.grid.core.domain.Balance
import com.gocarry.grid.core.domain.Grid
import com.gocarry.grid.test.core.domain.factory.FactoryDefaultValues.gridBalanceSet
import com.gocarry.grid.test.core.domain.factory.FactoryDefaultValues.gridDescription
import com.gocarry.grid.test.core.domain.factory.FactoryDefaultValues.gridId
import com.gocarry.grid.test.core.domain.factory.FactoryDefaultValues.gridUserCount

object GridFactory {
    fun create(id: String = gridId, description: String = gridDescription, userCount: Int = gridUserCount, balanceSet: MutableSet<Balance> = gridBalanceSet): Grid {
        return Grid.create(id, description, userCount, balanceSet)
    }
}