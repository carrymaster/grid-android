package com.gocarry.grid.test.core.domain.factory

import com.gocarry.grid.core.domain.Payment
import com.gocarry.grid.core.domain.UserBalance

object FactoryDefaultValues {
    const val gridId = "gridId"
    const val gridDescription = "the best grid"
    const val gridUserCount = 2
    const val expenseDescription = "the best expense"
    const val balanceAmount = 20.5f + 25.5f
    const val defaultUserName = "Melina Lapa"

    val paymentList = listOf(Payment.create(gridUserId(1), 20.5f), Payment.create(gridUserId(2), 25.5f))
    val userBalanceSet = setOf(UserBalance(gridUserId(1), -2.5f), UserBalance(gridUserId(2), 2.5f))
    val gridBalanceSet = mutableSetOf(BalanceFactory.create(userBalanceSet, balanceAmount))

    fun gridUserId(number: Int = 1) = "gridUserId$number"
    fun expenseId(number: Int = 1) = "expenseId$number"
}