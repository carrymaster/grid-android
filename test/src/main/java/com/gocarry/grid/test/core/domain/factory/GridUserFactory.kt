package com.gocarry.grid.test.core.domain.factory

import com.gocarry.grid.core.domain.GridUser
import com.gocarry.grid.test.core.domain.factory.FactoryDefaultValues.gridUserId

object GridUserFactory {
    fun create(id: String = gridUserId(), gridId: String = FactoryDefaultValues.gridId, userName: String = FactoryDefaultValues.defaultUserName): GridUser {
        return GridUser.create(id, userName, gridId)
    }
}