package com.gocarry.grid.test.extension

import org.mockito.Mockito

fun <T> anyKotlin(): T {
    Mockito.any<T>()
    return null as T
}