package com.gocarry.grid.test.core.domain.factory

import com.gocarry.grid.core.domain.Payment

object PaymentFactory {
    fun createList(userIdOne: String, userIdTwo: String, amountOne: Float = 25.5f, amountTwo: Float = 100f): List<Payment> {
        return listOf(Payment.create(userIdOne, amountOne), Payment.create(userIdTwo, amountTwo))
    }

    fun create(userId: String = FactoryDefaultValues.gridUserId(1), amount: Float = 25.5f): Payment {
        return Payment.create(userId, amount)
    }
}